<?php

use StoutLogic\AcfBuilder\FieldsBuilder;
use Yod\Globals\Acf;

// Instantiate Global Field
// We should bring it from another place
$designFields = new FieldsBuilder('global_fields');

$designFields
    ->addTab('design');

$designFieldsBuild = $designFields->build();
$designFieldsArray = $designFieldsBuild['fields'];


return [

    /*
    |--------------------------------------------------------------------------
    | Default Field Type Settings
    |--------------------------------------------------------------------------
    |
    | Here you can set default field group and field type configuration that
    | is then merged with your field groups when they are composed.
    |
    | This allows you to avoid the repetitive process of setting common field
    | configuration such as `ui` on every `trueFalse` field or your
    | preferred `instruction_placement` on every `fieldGroup`.
    |
    */

    'defaults' => [
        'trueFalse'  => ['ui' => 1],
        'select'     => ['ui' => 1],
        'textarea'   => ['new_lines' => 'br', 'rows' => 3],
        'image'      => ['return_format' => 'array', 'preview_size' => 'medium'],
        'datePicker' => ['return_format' => 'Ymd'],
    ],
    'globalfields' => [
        'design_tab' => [
            'key'       => $designFieldsBuild['key'],
            'fields'    => $designFieldsArray,
        ]
    ],
];
