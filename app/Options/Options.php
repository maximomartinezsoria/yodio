<?php

namespace App\Options;

use AcfGutenberg\Options as Field;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Yod\Globals\Acf;

class Options extends Field
{
    /**
     * The option page menu name.
     *
     * @var string
     */
    public $name = 'Options';

    /**
     * The option page document title.
     *
     * @var string
     */
    public $title = 'Options | Options';


    /**
     * The option page field group.
     *
     * @return array
     */
    public function fields()
    {
        $options = new FieldsBuilder('options');

        //TODO: create every Color picker based in the array of Yodio class colors
        $options
            ->addTab('Colors',['placement' => 'left'])
                ->addColorPicker('primary_color',[
                    'default_value' => 'blue',
                    'wrapper' => ['width' => '33%']
                ])
                ->addColorPicker('secondary_color',[
                    'default_value' => 'red',
                    'wrapper' => ['width' => '33%']
                ])
                ->addColorPicker('terciary_color',[
                    'default_value' => 'orange',
                    'wrapper' => ['width' => '33%']
                ])
                ->addColorPicker('black_color',[
                    'default_value' => '#333333',
                    'wrapper' => ['width' => '33%']
                ])
                ->addColorPicker('blue_color',[
                    'default_value' => '#084298',
                    'wrapper' => ['width' => '33%']
                ])
                ->addColorPicker('red_color',[
                    'default_value' => '#dc3545',
                    'wrapper' => ['width' => '33%']
                ])
                ->addColorPicker('orange_color',[
                    'default_value' => '#fd7e14',
                    'wrapper' => ['width' => '33%']
                ])
                ->addColorPicker('yellow_color',[
                    'default_value' => '#ffc107',
                    'wrapper' => ['width' => '33%']
                ])
                ->addColorPicker('green_color',[
                    'default_value' => '#198754',
                    'wrapper' => ['width' => '33%']
                ])
                ->addColorPicker('purple_color',[
                    'default_value' => '#6f42c1',
                    'wrapper' => ['width' => '33%']
                ])
                ->addColorPicker('pink_color',[
                    'default_value' => '#d63384',
                    'wrapper' => ['width' => '33%']
                ])
                ->addColorPicker('cyan_color',[
                    'default_value' => '#0dcaf0',
                    'wrapper' => ['width' => '33%']
                ])
                ->addColorPicker('gray_color',[
                    'default_value' => '#adb5bd',
                    'wrapper' => ['width' => '33%']
                ])
                ->addColorPicker('light_color',[
                    'default_value' => 'white',
                    'wrapper' => ['width' => '33%']
                ])
                ->addColorPicker('white_color',[
                    'default_value' => '#fff',
                    'wrapper' => ['width' => '33%']
                ])
                ->addColorPicker('link_color',[
                    'default_value' => 'blue',
                    'wrapper' => ['width' => '33%']
                ]);
        
        $options
            ->addTab('Typography',['placement' => 'left'])
                ->addSelect('heading_font', [
                    'label' => 'Heading Font',
                    'choices' => Acf::getFonts(),
                ])
                ->addSelect('text_font', [
                    'label' => 'Text Font',
                    'choices' => Acf::getFonts(),
                ])
                
                ->addNumber('body_fontsize', [
                    'label' => 'Body font size',
                    'default_value' => '16',
                        'min' => '12',
                        'max' => '20',
                ])

                ->addSelect('body_fontweight', [
                    'label' => 'Body font weight',
                    'choices'=> [
                        ['300' => '300'],
                        ['400' => '400'],
                        ['400' => '400'],
                    ],
                    'default_value' => '400'
                ])
                ->addGroup('h1',[
                    'label' => 'H1 settings'
                ])
                    ->addNumber('h1_min_size', [
                        'default_value' => '3',
                        'min' => '10',
                        'max' => '200',
                        'append' => 'px',
                        'wrapper' => [ 'width' => '50%' ],
                        'placeholder' => 'Minimun size',
                    ])
                    ->addNumber('h1_max_size', [
                        'default_value' => '8',
                        'min' => '10',
                        'max' => '200',
                        'append' => 'px',
                        'wrapper' => [ 'width' => '50%' ],
                        'placeholder' => 'Max size',
                    ])
                    ->addNumber('h1_letterspacing', [
                        'label' => 'Letter spacing',
                        'default_value' => '0',
                        'wrapper' => [ 'width' => '50%' ],
                        'min' => '-10',
                        'max' => '20',
                    ])
                    ->addSelect('h1_fontweight', [
                        'label' => 'Font weight',
                        'wrapper' => [ 'width' => '50%' ],
                        'choices' => ['300','400','500','600','700'],
                    ])
                ->endGroup()

                ->addGroup('h2',[
                    'label' => 'H2 settings'
                ])
                    ->addNumber('h2_min_size', [
                        'default_value' => '3',
                        'min' => '10',
                        'max' => '200',
                        'append' => 'px',
                        'wrapper' => [ 'width' => '50%' ],
                        'placeholder' => 'Minimun size',
                    ])
                    ->addNumber('h2_max_size', [
                        'default_value' => '8',
                        'min' => '10',
                        'max' => '200',
                        'append' => 'px',
                        'wrapper' => [ 'width' => '50%' ],
                        'placeholder' => 'Max size',
                    ])
                    ->addNumber('h2_letterspacing', [
                        'label' => 'Letter spacing',
                        'default_value' => '0',
                        'wrapper' => [ 'width' => '50%'],
                        'min' => '-10',
                        'max' => '20',
                    ])
                    ->addSelect('h2_fontweight', [
                        'label' => 'Font weight',
                        'wrapper' => [ 'width' => '50%'],
                        'choices' => ['300','400','500','600','700'],
                    ])
                ->endGroup()

                ->addGroup('h3',[
                    'label' => 'H3 settings'
                ])
                    ->addNumber('h3_min_size', [
                        'default_value' => '3',
                        'min' => '10',
                        'max' => '200',
                        'append' => 'px',
                        'wrapper' => [ 'width' => '50%' ],
                        'placeholder' => 'Minimun size',
                    ])
                    ->addNumber('h3_max_size', [
                        'default_value' => '8',
                        'min' => '10',
                        'max' => '200',
                        'append' => 'px',
                        'wrapper' => [ 'width' => '50%' ],
                        'placeholder' => 'Max size',
                    ])
                    ->addNumber('h3_letterspacing', [
                        'label' => 'Letter spacing',
                        'default_value' => '0',
                        'wrapper' => [ 'width' => '50%'],
                        'min' => '-10',
                        'max' => '20',
                    ])
                    ->addSelect('h3_fontweight', [
                        'label' => 'Font weight',
                        'wrapper' => [ 'width' => '50%'],
                        'choices' => ['300','400','500','600','700'],
                    ])
                ->endGroup()

                ->addGroup('h4',[
                    'label' => 'H4 settings'
                ])
                    ->addNumber('h4_min_size', [
                        'default_value' => '3',
                        'min' => '10',
                        'max' => '200',
                        'append' => 'px',
                        'wrapper' => [ 'width' => '50%' ],
                        'placeholder' => 'Minimun size',
                    ])
                    ->addNumber('h4_max_size', [
                        'default_value' => '8',
                        'min' => '10',
                        'max' => '200',
                        'append' => 'px',
                        'wrapper' => [ 'width' => '50%' ],
                        'placeholder' => 'Max size',
                    ])
                    ->addNumber('h4_letterspacing', [
                        'label' => 'Letter spacing',
                        'default_value' => '0',
                        'wrapper' => [ 'width' => '50%'],
                        'min' => '-10',
                        'max' => '20',
                    ])
                    ->addSelect('h4_fontweight', [
                        'label' => 'Font weight',
                        'wrapper' => [ 'width' => '50%'],
                        'choices' => ['300','400','500','600','700'],
                    ])
                ->endGroup()

                ->addGroup('h5',[
                    'label' => 'H5 settings'
                ])
                    ->addNumber('h5_min_size', [
                        'default_value' => '3',
                        'min' => '10',
                        'max' => '200',
                        'append' => 'px',
                        'wrapper' => [ 'width' => '50%' ],
                        'placeholder' => 'Minimun size',
                    ])
                    ->addNumber('h5_max_size', [
                        'default_value' => '8',
                        'min' => '10',
                        'max' => '200',
                        'append' => 'px',
                        'wrapper' => [ 'width' => '50%' ],
                        'placeholder' => 'Max size',
                    ])
                    ->addNumber('h5_letterspacing', [
                        'label' => 'Letter spacing',
                        'default_value' => '0',
                        'wrapper' => [ 'width' => '50%'],
                        'min' => '-10',
                        'max' => '20',
                    ])
                    ->addSelect('h5_fontweight', [
                        'label' => 'Font weight',
                        'wrapper' => [ 'width' => '50%'],
                        'choices' => ['300','400','500','600','700'],
                    ])
                ->endGroup()

                ->addGroup('h6',[
                    'label' => 'H6 settings'
                ])
                    ->addNumber('h6_min_size', [
                        'default_value' => '3',
                        'min' => '10',
                        'max' => '200',
                        'append' => 'px',
                        'wrapper' => [ 'width' => '50%' ],
                        'placeholder' => 'Minimun size',
                    ])
                    ->addNumber('h6_max_size', [
                        'default_value' => '8',
                        'min' => '10',
                        'max' => '200',
                        'append' => 'px',
                        'wrapper' => [ 'width' => '50%' ],
                        'placeholder' => 'Max size',
                    ])
                    ->addNumber('h6_letterspacing', [
                        'label' => 'Letter spacing',
                        'default_value' => '0',
                        'wrapper' => [ 'width' => '50%'],
                        'min' => '-10',
                        'max' => '20',
                    ])
                    ->addSelect('h6_fontweight', [
                        'label' => 'Font weight',
                        'wrapper' => [ 'width' => '50%'],
                        'choices' => ['300','400','500','600','700'],
                    ])
                ->endGroup()
                    
                ->addGroup('gutenberg_fontsizes')
                    ->addNumber('small_font_size', [
                        'label' => 'Small',
                        'default_value' => '12',
                        'append' => 'px',
                        'wrapper' => [ 'width' => '25%'],
                        'min' => '0',
                        'max' => '40',
                    ])
                    ->addNumber('regular_font_size', [
                        'label' => 'Regular',
                        'default_value' => '12',
                        'append' => 'px',
                        'wrapper' => [ 'width' => '25%'],
                        'min' => '0',
                        'max' => '40',
                    ])
                    ->addNumber('large_font_size', [
                        'label' => 'Large',
                        'default_value' => '12',
                        'append' => 'px',
                        'wrapper' => [ 'width' => '25%'],
                        'min' => '0',
                        'max' => '40',
                    ])
                    ->addNumber('huge_font_size', [
                        'label' => 'Huge',
                        'default_value' => '12',
                        'append' => 'px',
                        'wrapper' => [ 'width' => '25%'],
                        'min' => '0',
                        'max' => '40',
                    ])
                ->endGroup();

        $options
            ->addTab('Buttons',['placement' => 'left'])
                ->addNumber('btn_rounded', [
                    'label' => 'Button round border',
                    'instructions' => 'Size will be in pixels',
                    'min' => '0',
                    'max' => '100'
                ])
                ->addSelect('btn_font', [
                    'label' => 'Button Font',
                    'choices' => Acf::getFonts(),
                ])
                ->addNumber('btn_letterspacing',[
                    'label' => 'Letter spacing',
                    'min' => '0',
                    'max' => '10'
                ])

                //Button sm settings
                ->addGroup('btn_sm',['label' => 'Button small settings'])
                    ->addNumber('btn_sm_padding_x_size',[
                        'label' => 'Padding X',
                        'min' => '0',
                        'max' => '50',
                        'wrapper' => [ 'width' => '33%' ],
                    ])
                    ->addNumber('btn_sm_padding_y_size',[
                        'label' => 'Padding Y',
                        'min' => '0',
                        'max' => '50',
                        'wrapper' => [ 'width' => '33%' ],
                    ])
                    ->addNumber('btn_sm_font_size',[
                        'label' => 'Font size',
                        'min' => '8',
                        'max' => '50',
                        'wrapper' => [ 'width' => '33%' ],
                    ])
                ->endGroup()

                ->addGroup('btn_md',['label' => 'Button medium settings'])
                    ->addNumber('btn_md_padding_x_size',[
                        'label' => 'Padding X',
                        'min' => '0',
                        'max' => '50',
                        'wrapper' => [ 'width' => '33%' ],
                    ])
                    ->addNumber('btn_md_padding_y_size',[
                        'label' => 'Padding Y',
                        'min' => '0',
                        'max' => '50',
                        'wrapper' => [ 'width' => '33%' ],
                    ])
                    ->addNumber('btn_md_font_size',[
                        'label' => 'Font size',
                        'min' => '8',
                        'max' => '50',
                        'wrapper' => [ 'width' => '33%' ],
                    ])
                ->endGroup()

                ->addGroup('btn_lg',['label' => 'Button large settings'])
                    ->addNumber('btn_lg_padding_x_size',[
                        'label' => 'Padding X',
                        'min' => '0',
                        'max' => '50',
                        'wrapper' => [ 'width' => '33%' ],
                    ])
                    ->addNumber('btn_lg_padding_y_size',[
                        'label' => 'Padding Y',
                        'min' => '0',
                        'max' => '50',
                        'default_value' => '',
                        'wrapper' => [ 'width' => '33%' ],
                    ])
                    ->addNumber('btn_lg_font_size',[
                        'label' => 'Font size',
                        'min' => '8',
                        'max' => '50',
                        'default_value' => '30',
                        'wrapper' => [ 'width' => '33%' ],
                    ])
                ->endGroup()
                 
                ->addGroup('btn_hover')
                    ->addTrueFalse('shadow',[
                        'wrapper' => [ 'width' => '33%' ],
                        'default_value' => 0,
                    ])
                    ->addTrueFalse('brightness',[
                        'wrapper' => [ 'width' => '33%' ],
                        'default_value' => 0,
                    ])
                    ->addTrueFalse('grow',[
                        'wrapper' => [ 'width' => '33%' ],
                        'default_value' => 0,
                    ])
                ->endGroup();

            $options
                ->addTab('Identity',['placement' => 'left'])
                    ->addImage('logo_desktop',[
                        'return_format' => 'array',
                        'preview_size' => 'thumbnail',
                    ])
                    ->addImage('logo_mobile',[
                        'return_format' => 'array',
                        'preview_size' => 'thumbnail',
                    ]);
        
            $options
                ->addTab('Icons',['placement' => 'left'])
                    ->addSelect('icon_font', [
                        'choices' => [
                            'bootstrap-icons' => 'Bootstrap Icons',
                            // 'font-awesome' => 'Font Awesome',
                            // 'feathericons' => 'Feather Icons',
                            // 'octicons' => 'Octicons'
                        ],
                        'default' => 'bootstrap-icons'
                    ]);
            $options
                ->addTab('Content',['placement' => 'left'])
                    ->addCheckbox('enabled_post_types', [
                        'multiple' => 1,
                        'choices' => [
                            ['portfolio' => 'Portfolio'],
                            ['cases' => 'Cases'],
                            ['services' => 'Services'],
                        ],
                    ]);
            $options
                ->addTab('Cards Design',['placement' => 'left'])
                    ->addGroup('card_design')
                        ->addSelect('borders_radius', [
                            'choices'=> Acf::$border_radius,
                            'default_value' => 'rounded-0'
                        ])
                        ->addSelect('title_size', [
                            'choices'=> [
                                'h1'=> 'H1',
                                'h2'=> 'H2',
                                'h3'=> 'H3',
                                'h4'=> 'H4',
                                'h5'=> 'H5',
                                'h6'=> 'H6',
                            ],
                            'default_value' => 'h2'
                        ])
                    ->endGroup();
                
        return $options->build();
    }
}
