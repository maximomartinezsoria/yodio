<?php

/**
 * Theme filters.
 */

namespace App;


/**
 * Add "… Continued" to the excerpt.
 *
 * @return string
 */
add_filter('excerpt_more', function () {
    return sprintf(' &hellip; <a href="%s">%s</a>', get_permalink(), __('Continued', 'sage'));
});

add_filter('body_class', function (array $classes) {
    global $post;
    // Add GB Block class name
    if (isset($post->post_content) && has_blocks($post->post_content)) {
        $block_names = array_filter(array_column(parse_blocks($post->post_content), 'blockName'));
        foreach ($block_names as $block_name) {
            $name = preg_replace(['/^acf\//'], '', $block_name);
            $classes[] = 'has-' . $name;
        }
    }
    return array_filter($classes);
});


// add_action( 'admin_init', function(){
//     add_theme_support('editor-color-palette',Acf::CreateColorsForGutenberg());
// });


//Customize the admin bar menu
add_action('admin_bar_menu', function ($admin_bar){

    $admin_bar->remove_node('updates');
    $admin_bar->remove_node('comments');
    $admin_bar->remove_node('customize');
    $admin_bar->remove_node('updates');

    $admin_bar->add_menu( array(
        'id'    => 'block-areas',
        'title' => 'Block Areas',
        'href'  => admin_url() . 'edit.php?post_type=block-areas',
    ));

    $menus = get_posts(['post_type' => 'block-areas']);

    if(!empty($menus)) {
        foreach($menus as $menu){
            /* sub-menu */  
            $admin_bar->add_menu( array(  
                'parent' => 'block-areas',  
                'id'     => $menu->ID,
                'title' => 'Edit ' . $menu->post_title,
                'href'  => admin_url() . 'post.php?post='.$menu->ID.'&action=edit',
            )); 
        }
    } 
},90);

/**
 * Register a menu link Reusable blocks
 */

add_action( 'admin_menu', function() {
    add_menu_page(
        'Reusable Blocks',
        'Reusable Blocks',
        'edit_posts',
        'edit.php?post_type=wp_block',
        '',
        'dashicons-screenoptions',
        6
    );
});