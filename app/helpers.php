<?php

/**
 * Theme helpers.
 */

namespace App;

function enqueue_block_script($filename)
{
    wp_enqueue_script(
        slugify($filename),
        get_template_directory_uri() . '/public/scripts/' . $filename . '.js'
    );
}


function slugify($text)
{
    return strtolower(preg_replace('/[^A-Za-z0-9-]+/', '-', $text));
}

function upload_baseurl()
{
    $upload_path = wp_get_upload_dir();
    return $upload_path['baseurl'];
}
