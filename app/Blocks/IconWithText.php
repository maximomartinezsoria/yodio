<?php

namespace App\Blocks;

use AcfGutenberg\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;

class IconWithText extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Icon With Text';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = 'A simple Icon With Text block.';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'formatting';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'screenoptions';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = [];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => true,
        'align_text' => false,
        'align_content' => false,
        'anchor' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block styles.
     *
     * @var array
     */
    public $styles = [];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'iconbg' => $this->icon_bg(),
            'position_icon' => $this->position_icon(),
            'position_text' => $this->position_text()
        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $iconWithText = new FieldsBuilder('icon_with_text');

        $iconWithText
            ->addTab('content')
                ->addText('subtitle')
                ->addText('title')
                ->addImage('icon',[
                    'return_format' => 'array',
                    'preview_size' => 'thumbnail',
                ])
                ->addLink('link', [
                    'return_format' => 'array', /* 'array' || 'id' */
                ])
            ->addTab('design')
                ->addSelect('orientation',[
                    'choices' => [
                        'col-icon-text' => 'Icon left, content right',
                        'row-icon-text' => 'Icon top, content bottom'
                    ]
                ])
                ->addTrueFalse('icon_bg')
                ->addColorPicker('overlay_layer_color', [
                    'required' => 0,
                    'default_value' => '#000000'
                ])->conditional('icon_bg','==','1')
                ->addRange('icon_bg_size', [
                    'default_value' => '6',
                    'min' => '5',
                    'max' => '10',
                    'append' => 'rem'
                ])->conditional('icon_bg','==','1');

        return $iconWithText->build();
    }

    public function icon_bg(){
        return get_field('icon_bg') ? 'has_bg_icon' : "";
    }

    public function position_icon(){
        return (get_field('orientation') == 'col-icon-text') ? 'col-4' : 'col-12';
    }

    public function position_text(){
        return (get_field('orientation') == 'col-icon-text') ? 'col-8' : 'col-12';
    }
}
