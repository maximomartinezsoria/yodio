<?php

namespace App\Blocks;

use AcfGutenberg\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Yod\Globals\Acf;

class SocialIcons extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Social Icons';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = 'A simple Social Icons block.';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'formatting';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'share';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = [];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => true,
        'align_text' => false,
        'align_content' => false,
        'anchor' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block styles.
     *
     * @var array
     */
    public $styles = [
    ];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $example = [
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'border' => $this->border_wrapper()
        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $socialIcons = new FieldsBuilder('social_icons');

        $socialIcons
            ->addSelect('style_wrapper', [
                'choices'=> [
                    ['circle-border' => 'Circle border'],
                    ['square-border' => 'Square border'],
                    ['circle-fill' => 'Circle filled'],
                    ['square-fill' => 'Square filled'],
                    ['simple' => 'Only icon'],
                ],
                'default_value' => ''
            ])
            ->addSelect('color_wrapper', [
                'choices' => Acf::createColorSet('text'),
                'default_value' => ''
            ])
            ->addSelect('background_wrapper', [
                'choices' => Acf::createColorSet('bg'),
                'default_value' => ''
            ])
            ->addSelect('border_wrapper', [
                'choices' => Acf::createColorSet('border'),
                'default_value' => ''
            ])
            ->addSelect('vertical_margins', [
                'choices' => [
                    'my-0' => 'No margin',
                    'my-2' => 'Small margin',
                    'my-5' => 'Big margin'
                ],
                'default_value' => ''
            ])
            ->addRepeater('social')
                ->addSelect('network', [
                    'required' => true,
                    'choices'=> Acf::createSocialIcons()
                ])
                ->addLink('link', [
                    'required' => true,
                    'return_format' => 'array', /* 'array' || 'id' */
                ])
            ->endRepeater();

        return $socialIcons->build();
    }

    public function border_wrapper(){
        return (get_field('border_wrapper') == 'border-transparent') ? '' : get_field('border_wrapper') . ' border border-2';
    }

}
