<?php

namespace App\Blocks;

use AcfGutenberg\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Yod\Globals\Acf;

class Icon extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Icon';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = 'A simple Icon block.';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'formatting';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'insert';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = [];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => true,
        'align_text' => false,
        'align_content' => false,
        'anchor' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block styles.
     *
     * @var array
     */
    public $styles = [
    ];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $example = [
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'icon' => $this->icon(),
            'sizeicon' => $this->sizeicon(),
            'sizewrapper' => $this->sizewrapper(),
            'set_border' => $this->set_border(),
            'bs_classes' => $this->bs_classes()
        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $icon = new FieldsBuilder('icon');

        $icon
        ->addTab('content')
           ->addSelect('iconselect', [
               'choices'=> Acf::getSelectIcon(),
               'default_value' => 'bi bi-arrow-right'
           ])
           ->addSelect('style', [
               'choices'=> [
                   ['rounded' => 'Rounded'],
                   ['square' => 'Square'],
               ],
               'default_value' => ''
           ])
           ->addSelect('align', [
                'choices'=> [
                    ['justify-content-start' => 'Left'],
                    ['justify-content-center' => 'Center'],
                    ['justify-content-end' => 'Right'],
                ],
                'default_value' => 'justify-content-center'
            ])
           ->addColorPicker('color',[
               'default_value' => 'blue'
           ])
           ->addColorPicker('background',[
                'default_value' => 'transparent'
            ])
            ->addNumber('size_wrapper', [
                'default_value' => '10',
                'min' => '5',
                'max' => '200',
                'append' => 'px',
                'placeholder' => '',
            ])
            ->addColorPicker('border',[
                'default_value' => 'blue'
            ])
            ->addNumber('border_width', [
                'default_value' => '0',
                'min' => '0',
                'max' => '30',
                'placeholder' => '0',
            ])
            ->addNumber('size_icon', [
                'default_value' => '10',
                'min' => '5',
                'max' => '200',
                'append' => 'px',
                'placeholder' => '',
            ])
            
            ->addSelect('margins', [
                'choices'=> [
                    'mt-0' => 'mt-0',
                    'mt-1' => 'mt-1',
                    'mt-2' => 'mt-2',
                    'mt-3' => 'mt-3',
                    'mt-4' => 'mt-4',
                    'mt-5' => 'mt-5',
                    'mt-6' => 'mt-6',
                    'mt-7' => 'mt-7',
                    'mb-0' => 'mb-0',
                    'mb-1' => 'mb-1',
                    'mb-2' => 'mb-2',
                    'mb-3' => 'mb-3',
                    'mb-4' => 'mb-4',
                    'mb-5' => 'mb-5',
                    'mb-6' => 'mb-6',
                    'mb-7' => 'mb-7',
                ],
                'multiple' => true,
                'ui' => true,
                'default_value' => ''
            ]);

        return $icon->build();
    }

    /**
     * Return the items field.
     *
     * @return array
     */
    public function icon()
    {
        return get_field('iconselect') ?: '';
    }

    public function sizeicon()
    {
        return get_field('size_icon') ? get_field('size_icon') . 'px': '';
    }
    public function sizewrapper()
    {
        $size = get_field('size_wrapper');
        if($size) {
            return 'width: ' . $size . 'px; height: ' . $size . 'px';
        } else {
            return '';
        }
    }

    public function set_border(){
        if(get_field('border_width') != 0){
            return 'border-width: ' . get_field('border_width') . 'px;border-style:solid';
        } else {
            return '';
        }
    }

    public function bs_classes(){
       $classes = get_field('margins');
        if(is_array($classes)){
            return implode(' ',$classes);
        } else {
            return '';
        }
    }
}
