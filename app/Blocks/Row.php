<?php

namespace App\Blocks;

use AcfGutenberg\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Yod\Globals\Acf;

class Row extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Row';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = 'A simple Row block.';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'formatting';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'columns';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = [];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => true,
        'align_text' => false,
        'align_content' => false,
        'anchor' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block styles.
     *
     * @var array
     */
    public $styles = [
    ];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $example = [
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'data_columns' => $this->dataColumns()
        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $row = new FieldsBuilder('row');

        $row
            ->addTab('grid')
                ->addSelect('columns', [
                    'choices'=> [
                        ['1' => '1'],
                        ['2' => '2'],
                        ['3' => '3'],
                        ['4' => '4'],
                    ],
                    'default_value' => '1'
                ])
                ->addGroup('col_1')
                    ->addSelect('size_small', [
                        'choices'=> Acf::rowCols('data-col1-'),
                        'default_value' => 'data-col1-12',
                        'wrapper' => [ 'width' => '33.33%' ],
                    ])
                    ->addSelect('size_tablet', [
                        'choices'=> Acf::rowCols('data-col1-md-'),
                        'default_value' => 'data-col1-md-6',
                        'wrapper' => [ 'width' => '33.33%' ],
                    ])
                    ->addSelect('size_desktop', [
                        'choices'=> Acf::rowCols('data-col1-lg-'),
                        'default_value' => 'data-col1-lg-6',
                        'wrapper' => [ 'width' => '33.33%' ],
                    ])
                ->endGroup()

                ->addGroup('col_2')
                    ->conditional('columns','!=','1')
                    ->addSelect('size_small', [
                        'choices'=> Acf::rowCols('data-col2-'),
                        'default_value' => 'data-col2-lg-12',
                        'wrapper' => [ 'width' => '33.33%' ],
                    ])
                    ->addSelect('size_tablet', [
                        'choices'=> Acf::rowCols('data-col2-md-'),
                        'default_value' => 'data-col2-lg-6',
                        'wrapper' => [ 'width' => '33.33%' ],
                    ])
                    ->addSelect('size_desktop', [
                        'choices'=> Acf::rowCols('data-col2-lg-'),
                        'default_value' => 'data-col2-lg-6',
                        'wrapper' => [ 'width' => '33.33%' ],
                    ])
                ->endGroup()

                ->addGroup('col_3')
                    ->conditional('columns','!=','1')->and('columns','!=','2')
                    ->addSelect('size_small', [
                        'choices'=> Acf::rowCols('data-col3-'),
                        'default_value' => 'data-col3-lg-12',
                        'wrapper' => [ 'width' => '33.33%' ],
                    ])
                    ->addSelect('size_tablet', [
                        'choices'=> Acf::rowCols('data-col3-md-'),
                        'default_value' => 'data-col3-lg-6',
                        'wrapper' => [ 'width' => '33.33%' ],
                    ])
                    ->addSelect('size_desktop', [
                        'choices'=> Acf::rowCols('data-col3-lg-'),
                        'default_value' => 'data-col3-lg-6',
                        'wrapper' => [ 'width' => '33.33%' ],
                    ])
                ->endGroup()

                ->addGroup('col_4')
                    ->conditional('columns','==','4')
                    ->addSelect('size_small', [
                        'choices'=> Acf::rowCols('data-col4-'),
                        'default_value' => 'data-col4-lg-12',
                        'wrapper' => [ 'width' => '33.33%' ],
                    ])
                    ->addSelect('size_tablet', [
                        'choices'=> Acf::rowCols('data-col4-md-'),
                        'default_value' => 'data-col4-lg-6',
                        'wrapper' => [ 'width' => '33.33%' ],
                    ])
                    ->addSelect('size_desktop', [
                        'choices'=> Acf::rowCols('data-col4-lg-'),
                        'default_value' => 'data-col4-lg-6',
                        'wrapper' => [ 'width' => '33.33%' ],
                    ])
                ->endGroup()

                ->addSelect('inner_column_padding',[
                    'label' => 'Inner horizontal column padding',
                    'default_value' => 'inner-column-px-0',
                    'choices' => [
                        'inner-column-px-0' => 'No padding',
                        'inner-column-px-4' => 'Small padding',
                        'inner-column-px-6' => 'Medium padding',
                        'inner-column-px-10' => 'Big padding'
                    ]
                ])

                ->addSelect('inner_column_margin',[
                    'label' => 'Inner vertical column margin',
                    'default_value' => 'inner-column-px-0',
                    'choices' => [
                        'inner-column-my-0' => 'No margin',
                        'inner-column-my-4' => 'Small margin',
                        'inner-column-my-6' => 'Medium margin',
                        'inner-column-my-10' => 'Big margin'
                    ]
                    ]);
                $row->addTab('design')
                    ->addSelect('vertical_alignment', [
                        'choices'=> [
                            'columns-align-items-start' => 'Align to start',
                            'columns-align-items-center' => 'Align center',
                            'columns-align-items-end' => 'Align end',
                            'columns-align-items-stretch' => 'Align stretch'
                        ],
                        'default_value' => ''
                    ]);
                

        return $row->build();
    }

    public function dataColumns(){
        $col1 = get_field('col_1') ?: [];
        $col2 = get_field('col_2') ?: [];
        $col3 = get_field('col_3') ?: [];
        $col4 = get_field('col_4') ?: [];

        $classes = array($col1,$col2,$col3,$col4);

        $allclasses = [];
        foreach($classes as $class){
            if(is_array($class) && !empty($class)){
                foreach($class as $c){
                    $allclasses[] = $c;
                }
            }
        }

        $printclasses = implode(' ',$allclasses);

        if($printclasses == ''){
            $printclasses = 'data-col1-12 data-col1-md-12 data-col1-lg-12';
        }

        return $printclasses;
    }

}
