<?php

namespace App\Blocks;

use AcfGutenberg\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Log1x\Navi\Facades\Navi;
use App\Fields\Partials\Button;
use Yod\Globals\Acf;

class Header extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Header';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = 'A simple Header block.';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'formatting';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'menu';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = [];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = ['block-areas'];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The block styles.
     *
     * @var array
     */
    public $styles = [
    ];

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => true,
        'align_text' => false,
        'align_content' => false,
        'anchor' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'logo_desktop' => get_field('logo_desktop','options'),
            'logo_movile' => get_field('logo_mobile','options'),
            'mainnav' => $this->navigation(),
            'fontsize' => $this->font_size(),
            'fontweight' => $this->nav_bold()
        ];
    }

    public function getNavigation(){
        
        $menus = wp_get_nav_menus();

        $themenus = [];
        foreach($menus as $menu){
            $themenus[$menu->term_id] = $menu->name; 
        }

        return $themenus;
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $header = new FieldsBuilder('header');

        $header->addTab('Structure')
            ->addSelect('color_background',[
                'default_value' => 'bg-transparent',
                'choices' => Acf::createColorSet('bg')
            ])

            ->addSelect('navigation',[
                'choices' => $this->getNavigation(),
                'return_format'	=> 'value'
            ])

            ->addSelect('position',[
                'choices' => [
                    'position-relative' => 'Static',
                    'fixed-top' => 'Fixed on top',
                ]
            ])

            ->addSelect('alignment', [
                'label' => 'Horizontal alignment',
                'choices'=> [
                    ['justify-content-start' => 'Align start'],
                    ['justify-content-center' => 'Align center'],
                    ['justify-content-end' => 'Align end'],
                    ['justify-content-between' => 'Align space between'],
                ],
                'default_value' => ''
            ])

            ->addRepeater('buttons_navigation', [
                'layout' => 'block',
            ])
                ->addFields($this->get(Button::class))
            ->endRepeater();

        $header->addTab('design', [
                'placement' => 'top', /* 'top' || 'left' */
            ])

            ->addSelect('container', [
                'allow_null' => 0,
                'choices' => [
                    'container' => 'Container',
                    'container-fluid' => 'Full width'
                ]
            ])

            ->addSelect('expand_at', [
                'choices'=> [
                    ['sm' => 'Expand at SM'],
                    ['md' => 'Expand at MD'],
                    ['lg' => 'Expand at LG'],
                    ['xl' => 'Expand at XL'],
                    ['xxl' => 'Expand at XXL'],
                    ['never' => 'Never expand'],
                ],
                'default_value' => ''
            ])

            ->addSelect('nav_color_link',[
                'label' => 'Navigation link color',
                'choices' => Acf::createColorSet('text'),
                'default_value' => 'text-white'
            ])

            ->addSelect('nav_color_hover_link',[
                'label' => 'Navigation link hover/active color',
                'choices' => Acf::createColorSet('hover'),
                'default_value' => 'text-primary',
            ])

            ->addSelect('nav_style_scrolled',[
                'label' => 'Navigation color sticky navbar',
                'choices' => Acf::createColorSet('bg'),
                'default_value' => 'bg-white',
            ])

            ->addNumber('font_size', [
                'default_value' => '14',
                'min' => '8',
                'max' => '25',
                'placeholder' => '',
                'append' => 'px'
            ])

            ->addTrueFalse('nav_bold',[
                'label' => 'Nav link bold'
            ]);

        $header->addTab('mobile_design')

            ->addSelect('mobile_background_menu',[
                'default_value' => 'mobile-color-bg-primary',
                'choices' => Acf::createColorSet('mobile-color-bg'),
            ]);
        return $header->build();
    }

    /**
     * Returns the primary navigation.
     *
     * @return array
     */
    public function navigation()
    {
        $slug = get_field('navigation');
        if (Navi::build($slug)->isEmpty()) {
            return;
        }
        $nav = Navi::build($slug)->toArray();

        return $nav;
    }

    public function font_size(){
        return get_field('font_size') ? get_field('font_size') . 'px' : '14px';
    }

    public function nav_bold(){
        return (get_field('nav_bold')) ? 'fw-bold' : 'fw-normal';
    }

}
