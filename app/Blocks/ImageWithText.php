<?php

namespace App\Blocks;

use AcfGutenberg\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use App\Fields\Partials\Button;

class ImageWithText extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Image With Text';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = 'A simple Image With Text block.';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'formatting';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'align-left';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = [];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => true,
        'align_text' => false,
        'align_content' => false,
        'anchor' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block styles.
     *
     * @var array
     */
    public $styles = [

    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [

        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $imageWithText = new FieldsBuilder('image_with_text');

        $imageWithText
            ->addTab('content')
                ->addSelect('align_content',[
                    'label' => 'Align content vertically',
                    'choices' => [
                        'align-items-start' => 'Top',
                        'align-items-center' => 'Center',
                        'align-items-end' => 'Bottom',
                        'align-items-stretch' => 'Stretch'
                    ]
                ])
                ->addImage('image', [
                    'return_format' => 'array', /* 'array' || 'id' || 'url' */
                    'preview_size' => 'large',
                ])
            ->addTab('design')
                ->addSelect('order',[
                    'choices' => [
                        'image-text' => 'Image left, content right',
                        'text-image' => 'Content left, image right'
                    ]
                ]);

        return $imageWithText->build();
    }
}
