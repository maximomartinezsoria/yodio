<?php

namespace App\Blocks;

use AcfGutenberg\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Yod\Globals\Acf;

class ButtonCustom extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Button Custom';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = 'A simple Button Custom block.';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'formatting';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'button';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = [];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => true,
        'align_text' => false,
        'align_content' => false,
        'anchor' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block styles.
     *
     * @var array
     */
    public $styles = [
    ];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $example = [
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $buttonCustom = new FieldsBuilder('button_custom');

        $buttonCustom
        ->addTab('content')
            ->addLink('link', [
                'return_format' => 'array', /* 'array' || 'id' */
            ])
            ->addSelect('style',[
                'allow_null' => 0,
                'choices' => Acf::createColorSet('bg'),
                'default_value' => 'bg-primary',
            ])
            ->addSelect('color',[
                'allow_null' => 0,
                'choices' => Acf::createColorSet('text'),
                'default_value' => 'text-white',
            ])
            ->addSelect('size',[
                'allow_null' => 1,
                'choices' => [
                    'btn-md' => 'Normal',
                    'btn-sm' => 'Small',
                    'btn-lg' => 'Large',
                ],
                'default_value' => ['none'],
            ]);

        return $buttonCustom->build();
    }
}
