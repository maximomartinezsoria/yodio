<?php

namespace App\Blocks;

use AcfGutenberg\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Card extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Card';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = 'A simple Card block.';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'formatting';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'tide';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = ['card'];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = ['block-areas'];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => true,
        'align_text' => true,
        'align_content' => true,
        'anchor' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block styles.
     *
     * @var array
     */
    public $styles = [
    ];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $example = [
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            
        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $card = new FieldsBuilder('card');

        $card
            ->addSelect('the_post_type', [
                'choices' => [
                    'post' => 'Post',
                    'events' => 'Events'
                ],
                'default_value' => 'default_value',
            ]);

        return $card->build();
    }
}
