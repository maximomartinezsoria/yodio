<?php

namespace App\Blocks;

use AcfGutenberg\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use App\Fields\Partials\Button;

class Hero extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Hero';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = 'The Hero block.';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'formatting';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'table-row-before';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = [];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => true,
        'align_text' => true,
        'align_content' => true,
        'anchor' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block styles.
     *
     * @var array
     */
    public $styles = [
        
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'shadow' => $this->inset_shadow(),
            'has_bgimg' => $this->has_bgimg(),
            'uppertextstyle' => $this->uppertext_style(),
            'bgimg' => $this->bg_img(),
            'layer_blendmode' => $this->blend_mode()
        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $hero = new FieldsBuilder('hero');

        $hero
            ->addTab('Content')
            ->addTrueFalse('innerblock',[
                'label' => 'Custom Block content',
            ])
            ->addText('heading', [
                'default_value' => '',
            ])->conditional('innerblock', '==', false)

            ->addText('subheading', [
                'default_value' => '',
            ])->conditional('innerblock', '==', false)

            ->addText('uppertext', [
                'default_value' => '',
            ])->conditional('innerblock', '==', false)
            
            ->addSelect('uppertext_style',[
                'allow_null' => 0,
                'choices' => [
                    'none' => 'Normal',
                    'bg-primary' => 'With primary color background',
                    'bg-secondary' => 'With secondary color background',
                    'rounded-primary' => 'Rounded with primary color background',
                    'rounded-secondary' => 'Rounded with secondary color background',
                    
                ],
                'default_value' => ['none'],
            ])->conditional('innerblock', '==', false)

            ->addWysiwyg('content', [
                'default_value' => '',
                'toolbar' => 'basic',
                'media_upload' => 0,
            ])->conditional('innerblock', '==', false)
            
            //Layer styles
            ->addTrueFalse('overlay_layer')
            ->addColorPicker('overlay_layer_color', [
                'required' => 0,
                'default_value' => '#000000',
                'wrapper' => ['width' => '50%']
            ])
            ->conditional('overlay_layer', '==', '1')

            ->addNumber('overlay_layer_opacity', [
                'min' => 0,
                'max' => 1,
                'required' => 0,
                'default_value' => 0.8,
                'wrapper' => ['width' => '50%']
            ])->conditional('overlay_layer', '==', '1')

            ->addSelect('blend_mode', [
                    'allow_null' => 0,
                    'choices' => [
                        'normal' => 'Normal',
                        'multiply' => 'Multiply',
                        'screen' => 'Screen',
                        'overlay' => 'Overlay',
                        'darken' => 'Darken',
                        'lighten' => 'Lighten',
                        'color-dodge' => 'Color-dodge',
                        'color-burn' => 'Color-burn',
                        'hard-light' => 'Hard-light',
                        'soft-light' => 'Soft-light',
                        'difference' => 'Difference',
                        'exclusion' => 'Exclusion',
                        'hue' => 'Hue',
                        'saturation' => 'Saturation',
                        'color' => 'Color'
                    ]
                ])

            ->addTrueFalse('inset_shadow',[
                'instructions' => 'Creates an inset shadow in the main container',
            ])

            ->addFields($this->get(Button::class))

            ->addImage('bg_image', [
                'return_format' => 'array', /* 'array' || 'id' || 'url' */
                'preview_size' => 'large',
            ])
            ->setInstructions('Image to display in the background')

            ->addTab('design')
                ->addSelect('content_alignment', [
                    'allow_null' => 0,
                    'choices' => [
                        'container-align-left' => 'Left',
                        'container-align-right' => 'Right',
                        'container-align-center' => 'Center',
                        'container-align-center-narrow' => 'Center narrow',
                    ]
                ])

                ->addSelect('height',[
                    'label' => 'Vertical padding/height',
                    'choices' => [
                        'hv-100' => 'Full screen',
                        'h-100' => '100% container',
                        'py-10' => 'Taller',
                        'py-9' => 'Tall',
                        'py-8' => 'Medium',
                        'py-6' => 'Small',
                        'py-3' => 'Smaller',
                        'py-0' => 'No padding'
                    ],
                    'default_value' => 'py-0'
                ]);
            
        
        return $hero->build();
    }

    /**
     * Return if bg image was selected field.
     *
     * @return array
     */
    public function has_bgimg()
    {
        return get_field('bg_image') ? "has-bgimage" : '';
    }

    /**
     * Return bg image
     *
     * @return array
     */
    public function bg_img()
    {
        return get_field('bg_image') ?: ['url' => ''];
    }

    /**
     * Return the inset_shadow field.
     *
     * @return string
     */
    public function inset_shadow()
    {
        return get_field('inset_shadow') ? "inset-shadow" : '';
    }

    /**
     * Return the overlay blendmode field.
     *
     * @return string
     */
    public function blend_mode()
    {
        return get_field('blend_mode') ? 'blendmode-' . get_field('blend_mode') : '';
    }
    

    /**
     * Return the uppertext_style field.
     *
     * @return string
     */
    public function uppertext_style()
    {   
        switch(get_field('uppertext_style')) :
            case "bg-primary" :
                return "bg-primary text-white";
                break;
            case "bg-secondary" :
                    return "bg-secondary text-white";
                    break;
            case "rounded-primary" :
                return "bg-primary border-radius text-white";
                break;
            case "rounded-secondary" :
                return "bg-secondary border-radius text-white";
                break;
            default :
                return "";
                break;
        endswitch;
    }
}
