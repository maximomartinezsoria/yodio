<?php

namespace App\Blocks;

use AcfGutenberg\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Log1x\Navi\Facades\Navi;
use Yod\Globals\Acf;


class NavMenu extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Nav Menu';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = 'A navigation menu block.';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'formatting';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'list-view';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = [];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => true,
        'align_text' => false,
        'align_content' => false,
        'anchor' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block styles.
     *
     * @var array
     */
    public $styles = [
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'selected_nav' => $this->navigation(),
            'fontsize' => $this->font_size(),
            'fontweight' => $this->nav_bold()
        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $navMenu = new FieldsBuilder('nav_menu');

        $navMenu
            ->addSelect('menu',[
                'choices' => $this->getNavigation(),
                'return_format'	=> 'value'
            ])
            ->addSelect('color_link',[
                'label' => 'Menu link color',
                'choices' => Acf::createColorSet('text'),
                'default_value' => 'text-white'
            ])

            ->addSelect('color_hover_link',[
                'label' => 'Menu link hover/active color',
                'choices' => Acf::createColorSet('hover'),
                'default_value' => 'text-primary',
            ])
            ->addNumber('font_size', [
                'default_value' => '14',
                'min' => '8',
                'max' => '25',
                'placeholder' => '',
                'append' => 'px'
            ])

            ->addTrueFalse('nav_bold',[
                'label' => 'Nav link bold'
            ]);

        return $navMenu->build();
    }

    public function getNavigation(){
        
        $menus = wp_get_nav_menus();

        $themenus = [];
        foreach($menus as $menu){
            $themenus[$menu->term_id] = $menu->name; 
        }

        return $themenus;
    }

    public function font_size(){
        return get_field('font_size') ? get_field('font_size') . 'px' : '14px';
    }

    public function nav_bold(){
        return (get_field('nav_bold')) ? 'fw-bold' : 'fw-normal';
    }


    /**
     * Returns the all menues created.
     *
     * @return array
     */
    public function navigation()
    {
        $slug = get_field('menu');
        if (Navi::build($slug)->isEmpty()) {
            return;
        }
        $nav = Navi::build($slug)->toArray();

        return $nav;
    }
}
