<?php

namespace App\Blocks;

use AcfGutenberg\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Query extends Block {
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Query';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = 'Query block.';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'formatting';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'images-alt';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = [];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => true,
        'align_text' => false,
        'align_content' => false,
        'anchor' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block styles.
     *
     * @var array
     */
    public $styles = [];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $example = [
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with() {
        return [
            'section_title' => get_field('section_title'),
            'section_intro' => get_field('section_intro'),
            'items' => $this->query(),
            'columns' => get_field('columns'),
        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields() {
        $query = new FieldsBuilder('query');

        $query
            //Query tab
            ->addTab('query')
            ->addSelect('post_type', [
                'choices' => $this->postType(),
                'default_value' => '',
                'ajax' => 1,
                'ui' => 1,
            ])

            ->addGroup('tax_query', [
                'label' => 'Filter by Taxonomies',
                'layout' => 'block', /* 'table' || 'row' || 'block' */
                'wrapper' => [
                    'width' => '75%',
                ]
            ])
                ->addSelect('relation', [
                    'choices'=> [
                        ['AND' => 'AND'],
                        ['OR' => 'OR'],
                    ],
                    'default_value' => ''
                ])
                ->addRepeater('tax_queries', [
                    'layout' => 'block', /* 'table' || 'row' || 'block' */
                    'instructions' => 'Note: If you select a taxonomy to filter results by, the filter will only search through entries that are within that taxonomy term.'
                ])
                    ->addSelect('taxonomy_name', [
                        'choices'=> [
                        ],
                        'ui' => 1,
                        'ajax' => 1,
                        'default_value' => '',
                        'wrapper' => [
                            'width' => '33.333%',
                            'class' => 'custom-class',
                        ]
                    ])
                    ->addSelect('selected_terms', [
                        'choices'=> [
                        ],
                        'multiple' => 1,
                        'ui' => 1,
                        'ajax' => 1,
                        'wrapper' => [
                            'width' => '33.333%',
                            'class' => 'custom-class',
                        ]
                    ])
                ->endRepeater()
            ->endGroup()

            ->addSelect('columns', [
                'choices' => [
                    ['12' => '1'],
                    ['6' => '2'],
                    ['4' => '3'],
                    ['3' => '4'],
                ],
                'default_value' => '12',
                'wrapper' => [
                    'width' => '50%'
                ]
            ])
            ->addSelect('column_width', [
                'choices'=> [
                    ['col-12' => 'Full'],
                    ['col-12 col-md-10 mx-auto' => '90%'],
                    ['col-12 col-md-9 mx-auto' => '70%'],
                    ['col-12 col-md-6 mx-auto' => '50%'],
                ],
                'default_value' => ''
            ])
            ->addNumber('items_count', [
                'default_value' => '3',
                'min' => '-1',
                'max' => '',
                'step' => '1',
                'placeholder' => '',
                'wrapper' => [
                    'width' => '50%'
                ]
            ])
            ->addSelect('orderby', [
                'choices' => [
                    ['none' => 'None'],
                    ['ID' => 'ID'],
                    ['author' => 'author'],
                    ['title' => 'title'],
                    ['name' => 'name'],
                    ['date' => 'date'],
                    ['modified' => 'modified'],
                    ['parent' => 'parent'],
                    ['rand' => 'rand'],
                    ['relevance' => 'relevance'],
                    ['menu_order' => 'menu_order'],
                ],
                'default_value' => 'title',
                'wrapper' => [
                    'width' => '50%'
                ]
            ])
            ->addSelect('order', [
                'choices' => [
                    ['ASC' => 'ASC'],
                    ['DESC' => 'DESC'],
                ],
                'default_value' => 'DESC',
                'wrapper' => [
                    'width' => '50%'
                ]
            ])

            ->addTab('card', [
                'placement' => 'top', /* 'top' || 'left' */
            ])

            ->addSelect('card_type', [
                'choices' => [
                    ['simple_card' => 'Simple'],
                    ['image_with_text_card' => 'Image with text'],
                ],
                'default_value' => 'simple_card',
                'wrapper' => [
                    'width' => '50%'
                ]
            ])

            ->addTrueFalse('show_excerpt', [
                'default_value' => 0,
                'ui' => 1,
            ])
            
            //Taxonomy
            ->addTrueFalse('show_taxonomy', [
                'default_value' => 0,
                'ui' => 1,
            ])
            ->addSelect('taxonomy_field', [
                'choices'=> [],
                'ui' => 1,
                'ajax' => 1,
                'default_value' => ''
            ])->conditional('show_taxonomy','==',1)

            ->addSelect('taxonomy_position', [
                'choices'=> [
                    ['top' => 'Top'],
                    ['above-title' => 'Above title'],
                    ['below-title' => 'Below title'],
                ],
                'default_value' => ''
            ])->conditional('show_taxonomy','==',1)
            
            ->addTrueFalse('show_date', [
                'default_value' => 0,
                'ui' => 1,
            ])
            ->addSelect('date_position', [
                'choices'=> [
                    ['top' => 'Top'],
                    ['above-title' => 'Above title'],
                    ['below-title' => 'Below title'],
                ],
                'default_value' => ''
            ])->conditional('show_date','==',1);

        return $query->build();
    }
    

    public function postType() {
        $post_types = [];

        if(get_field('enabled_post_types', 'options')) {
            $cpts = get_field('enabled_post_types', 'options');
            $post_types = array_merge(array('posts' => 'Posts'),$cpts);
        }

        return $post_types;
    }

    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        \App\enqueue_block_script('Query');
    }

    public function query() {
        if (!get_field('post_type')) {
            return false;
        }

        $posts = [];

        $args = [
            'post_type' => get_field('post_type'),
            'posts_per_page' => get_field('items_count'),
            'orderby' => get_field('orderby'),
            'order' => get_field('order'),
        ];

        // Check if There is a repeater field
        if (get_field('tax_query')) {
            $taxonomies = $this->taxonomies(get_field('tax_query'));

            if(!is_null($taxonomies))
                $args = array_merge($args, $taxonomies);
        }


        $items = new \WP_Query($args);

        if ($items->have_posts()) {
            $posts = collect($items->posts);

            $posts = $posts->map(function ($post) {
                return $post->ID;
            });
        }

        return $posts;
    }

    /**
     * Return the taxonomies query.
     *
     * @return array
     */
    private function taxonomies($taxonomies)
    {
        if ($taxonomies['tax_queries'] && !is_null($taxonomies['tax_queries']) && count($taxonomies['tax_queries']) > 0) {

            $args['tax_query'] = [
                'relation' => $taxonomies['relation'],
            ];

            // Check if repeater is valid
            foreach ($taxonomies['tax_queries'] as $single_tax_query) {

                $args['tax_query'][] = [
                    'taxonomy' =>  $single_tax_query['taxonomy_name'],
                    'field' =>  'slug',
                    'terms' =>  $single_tax_query['selected_terms'],
                ];
            }

            return $args;
        }
    }
}
