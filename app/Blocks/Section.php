<?php

namespace App\Blocks;

use AcfGutenberg\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Yod\Globals\Acf;

class Section extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Section';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = 'A simple Section block.';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'formatting';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'embed-generic';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = [];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => false,
        'align_text' => true,
        'align_content' => false,
        'anchor' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block styles.
     *
     * @var array
     */
    public $styles = [];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $example = [];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'img_bg' => $this->image_bg_file()
        ];
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {
        $section = new FieldsBuilder('section');

        $section
            ->addTab('settings', [
                'placement' => 'top', /* 'top' || 'left' */
            ])
                ->addSelect('container', [
                    'allow_null' => 0,
                    'choices' => [
                        'container' => 'Container',
                        'container-fluid' => 'Full width'
                    ]
                ])
                ->addSelect('height',[
                    'label' => 'Vertical padding/height',
                    'choices' => [
                        'hv-100' => 'Full screen',
                        'h-100' => '100% container',
                        'py-10' => 'Taller',
                        'py-9' => 'Tall',
                        'py-8' => 'Medium',
                        'py-6' => 'Small',
                        'py-3' => 'Smaller',
                        'py-0' => 'No padding'
                    ],
                    'default_value' => 'py-0'
                ])

                ->addSelect('horizontal_alignment', [
                    'choices'=> [
                        'text-center' => 'Center',
                        'text-right' => 'Right',
                        'text-left' => 'Left'
                    ],
                    'default_value' => ''
                ])

                ->addTab('design', [
                    'placement' => 'top', /* 'top' || 'left' */
                ])

                ->addSelect('bg_color', [
                    'allow_null' => 1,
                    'choices' => Acf::createColorSet('bg'),
                ])

                ->addTrueFalse('image_background')

                ->addImage('image_bg_file',[
                    'return_format' => 'array',
                    'preview_size' => 'thumbnail',
                ])->conditional('image_background','==',1)

                ->addTrueFalse('image_grayscale')
                    ->conditional('image_background','==',1)
                ->addNumber('image_background_opacity', [
                    'default_value' => '0.2',
                    'min' => '0',
                    'max' => '1',
                    'step' => '0.1',
                ])->conditional('image_background','==',1)

                ->addText('custom_classes')
                ->addText('custom_id');

        return $section->build();
    }

        /**
     * Return bg image
     *
     * @return array
     */
    public function image_bg_file()
    {
        return get_field('image_bg_file') ?: ['url' => ''];
    }



    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        //
    }
}
