<?php

/**
 * Theme filters.
 */

namespace App;
namespace Yod\Globals;

// Add the color defined in general themes
add_action('acf/input/admin_footer', function(){ ?>
    <script type="text/javascript">
    (function($) {
        acf.add_filter('color_picker_args', function( args, $field ){
        // add the hexadecimal codes here for the colors you want to appear as swatches
        args.palettes = <?php echo Acf::CreateColorsForAcf();?>
        // return colors
        return args;
        });
    })(jQuery);
    </script>
<?php });

//Get the post type selected in Quqery block
add_filter('acf/load_field/name=post_type', function ($field) {
    $posts_types = get_post_types([
        'public' => true,
        '_builtin' => false
    ], 'object');

    $field['choices'] = [];

    $field['choices']['post'] = 'Post';

    foreach ($posts_types as $post_type) {
        $field['choices'][$post_type->name] = $post_type->label;
    }

    unset($field['choices']['block-areas']);

    return $field;
});

/**
 * https://support.advancedcustomfields.com/forums/topic/send-additional-field-for-ajax-update-event-on-selecttaxonomy/
 */
add_filter('acf/load_field/name=taxonomy_name', function ($field) {
    // Look for the post type in the AJAX request.
    $post_type = isset($_REQUEST['post_type']) ? $_REQUEST['post_type'] : false;
    if (!$post_type) {
        return $field;
    }

    $taxonomies = get_object_taxonomies($post_type);

    $field['choices'] = [];

    foreach ($taxonomies as $taxonomy) {
        $tax_object = get_taxonomy($taxonomy);
        $field['choices'][$taxonomy] = $tax_object->labels->name;
    }

    return $field;
},9999);

add_filter('acf/prepare_field/name=taxonomy_name', function ($field) {
    $post_type = get_field('post_type');

    if (!$post_type) {
        return $field;
    }

    $taxonomies = get_object_taxonomies($post_type);


    $field['choices'] = [];

    foreach ($taxonomies as $taxonomy) {
        $tax_object = get_taxonomy($taxonomy);
        $field['choices'][$taxonomy] = $tax_object->labels->name;
    }

    return $field;
});

/**
 * Show Fields if any term is selected
 */
add_filter('acf/prepare_field/name=selected_terms', function ($field) {

    // Find the current Index of the subfield
    preg_match('/row-(\d+)/', $field['prefix'], $matches);
    if(!$matches) return null;

    $row = $matches[1];

    // Look for the sibling taxonomy object
    $current_taxonomy = get_field_object('tax_query_tax_queries_' . $row . '_taxonomy_name');

    // If there is a value for that field look for the object using get_term_by function
    if ($current_taxonomy['value']) {

        foreach ($field['value'] as $slug) {
            $term_object = get_term_by('slug', $slug, $current_taxonomy['value']);
            if ($term_object) {
                $field['choices'][$slug] = $term_object->name;
            }
        }
    }

    return $field;
});

/**
 * Dynamically load Selected Terms subfield
 * based on chosen taxonomy and value that's coming from
 * AJAX via acf.add_filter('select2_ajax_data')
 */
add_filter('acf/load_field/name=selected_terms', function ($field) {

    // Look for the taxonomy name in the AJAX request.
    $taxonomy_name = isset($_REQUEST['taxonomy_name']) ? $_REQUEST['taxonomy_name'] : false;

    if (!$taxonomy_name) {
        return $field;
    }

    $terms = get_terms($taxonomy_name, array(
        'hide_empty' => false,
    ));

    // If not found, use the previous vehicle's value.
    $field['choices'] = [];

    foreach ($terms as $term) {
        $field['choices'][$term->slug] = $term->name;
    }

    // $field['choices']['hello'] = $taxonomy_name;

    return $field;
});


add_filter('acf/load_field/key=field_query_taxonomy_field', function ($field) {
    // Look for the post type in the AJAX request.
    $post_type = isset($_REQUEST['post_type']) ? $_REQUEST['post_type'] : false;

    if (!$post_type) {
        return $field;
    }

    $taxonomies = get_object_taxonomies($post_type);

    $field['choices'] = [];

    foreach ($taxonomies as $taxonomy) {
        $tax_object = get_taxonomy($taxonomy);
        $field['choices'][$taxonomy] = $tax_object->labels->name;
    }

    // $data = get_posts(['post_type' => $post_type,'posts_per_page' => 1]);
    // $metafields = get_post_meta($data[0]->ID);

    // foreach($metafields as $i => $metafield) {
    //     if(!str_starts_with($i,'_') && !str_contains($i,'rank')) {
    //         $field['choices'][$i] = ucwords($i);
    //     }
    // }


    return $field;
},9999);


add_filter('acf/prepare_field/key=field_query_taxonomy_field', function ($field) {

    $tax = get_field('taxonomy_field');

    if (!$tax) {
        return $field;
    }

    $taxonomy = get_taxonomy($tax);

    if($taxonomy) {
        $field['choices'][$taxonomy->name] = $taxonomy->label;
    }

    return $field;
});