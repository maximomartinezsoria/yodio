<?php

/**
 * Blocks filters.
 */

namespace App;

function hasManualColor($str) {
    return (strpos('#', $str) !== false) ? true : false;
}

/**
 * Modify render block classes to BS utilities
 * */
add_filter( 'render_block', function( $block_content, $block) {
    
    $block_attr = $block['attrs'];

    //Modify Text align Gutenberg classes
    if ( isset( $block_attr['textAlign'] )) {

        $align = $block_attr['textAlign'];

        $block_content = str_replace( 'has-text-align-' . $align, 'text-' . $align, $block_content );
    }

    if ( isset( $block_attr['textColor'] ) && 
        !hasManualColor($block_attr['textColor'])) {

        $color = $block_attr['textColor'];

        $block_content = str_replace( 'has-'.$color.'-color', 'text-' . $color, $block_content );
    }

    if ( isset( $block_attr['backgroundColor'] ) && 
    !hasManualColor($block_attr['backgroundColor']) ) {

        $color = $block_attr['backgroundColor'];

        $block_content = str_replace( 'has-'.$color.'-background-color', 'bg-' . $color, $block_content );

    }

	return $block_content;
},10,2);