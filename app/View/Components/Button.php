<?php

namespace App\View\Components;

use Roots\Acorn\View\Component;

class Button extends Component
{
    public $button;
    public $buttonClass;

    /**
     * Create a new component instance.
     *
     * @param  array    $button
     * @param  boolean  $buttonClass
     * @return void
     */
    public function __construct($button = [], $buttonClass = '' )
    {
        $this->button = $button;
        $this->buttonClass = $buttonClass;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return $this->view('components.button');
    }
}
