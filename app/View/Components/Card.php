<?php

namespace App\View\Components;

use Roots\Acorn\View\Component;

class Card extends Component {
    public $title;
    public $thumbnail;
    public $link;
    public $rounded;
    public $title_size;
    public $description = false;
    public $taxonomy = false;
    public $date = false;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($card = [], $postId = null, $type = '', $orientation = null, $showexcerpt = true) {
        $this->type = get_field('card_type') ?? 'simple_card';
        $this->link = get_permalink($postId);
        $this->type = get_field('card_type') ?? 'simple_card';
        
        $design = get_field('card_design','options') ?: [];

        $this->rounded = $design['borders_radius'] ?: '';
        $this->title_size = $design['title_size'] ?: '';

        if ($postId) {
            if (has_post_thumbnail($postId)) {
                $imageThumbnail = get_the_post_thumbnail_url($postId);
            }

            if(get_field('show_excerpt') == 1){
                $this->description = get_the_excerpt($postId);
            }

            if(get_field('show_taxonomy') == 1){
                $terms = get_the_terms($postId, get_field('taxonomy_field'));
                if($terms) {
                    $this->taxonomy = $terms[0]->name;
                }
            }

            if(get_field('show_date') == 1){
                $this->date = get_the_time('d-m-Y');
            }

            $this->thumbnail = $imageThumbnail ?? null;

            $this->title = get_the_title($postId);
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render() {
        if ($this->type && $this->type !== '') {
            return $this->view('components.cards.' . $this->type);
        }

        return $this->view('components.card');
    }
}
