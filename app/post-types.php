<?php

add_action( 'init', function() {

	$enabled_post_types = get_field('enabled_post_types', 'options') ?: [];

	register_extended_post_type( 'block-areas', [
		'show_in_feed' => false,
		'menu_icon' => 'dashicons-editor-table',
		'show_in_rest' => true,
		'archive' => [
			'nopaging' => true,
		],
		'dashboard_activity' => false,

	], [
		'singular' => 'Block Area',
		'plural'   => 'Blocks Areas',
		'slug'     => 'block-areas',
	]);

	// Category
	register_extended_taxonomy('area-type', ['block-areas'], [
		'public' => true,
		'publicly_queryable' => true,
		'hierarchical' => true,
		'show_in_rest' => true,
		'show_ui' => true
	], [
		'singular' => 'Type',
		'plural'   => 'Types'
	]);


	if (in_array('cases', $enabled_post_types)) {
		register_extended_post_type( 'cases', [
			'show_in_feed' => true,
			'menu_icon' => 'dashicons-groups',
			'show_in_rest' => true,
			'archive' => [
				'nopaging' => true,
			],
			'dashboard_activity' => false,

		], [
			'singular' => 'Case',
			'plural'   => 'Cases',
			'slug'     => 'case',

		]);
	}

	if (in_array('services', $enabled_post_types)) {
		register_extended_post_type( 'services', [
			'show_in_feed' => true,
			'menu_icon' => 'dashicons-groups',
			'show_in_rest' => true,
			'archive' => [
				'nopaging' => true,
			],
			'dashboard_activity' => false,

		], [
			'singular' => 'Service',
			'plural'   => 'Services',
			'slug'     => 'service',

		]);

		// Category
		register_extended_taxonomy('locations', ['posts','services'], [
			'public' => true,
			'publicly_queryable' => true,
			'hierarchical' => true,
			'show_in_rest' => true,
			'show_ui' => true
		], [
			'singular' => 'Location',
			'plural'   => 'Locations'
		]);
	}
},10);
