<?php

namespace App;
use function Roots\view;

/**
 * Register Block Pattern Categories.
 */
if ( function_exists( 'register_block_pattern_category' ) ) {

    add_action( 'init', function() {

        //Define the categories to add
        $categories = ['hero', 'sliders', 'cards'];

        foreach($categories as $cat) :
            register_block_pattern_category(
                $cat,
                array( 'label' => ucwords(str_replace('_',' ',$cat)))
            );
        endforeach;
    });

}

// Add the default blocks location, 'views/blocks-patterns', via filter
add_filter('sage-blocks-patterns-folder', function () {
    return ['views/blocks-patterns'];
});

/**
 * Function to strip the `.blade.php` from a blade filename
 */
function removeBladeExtension($filename)
{
    // Filename must end with ".blade.php". Parenthetical captures the slug.
    $blade_pattern = '/(.*)\.blade\.php$/';
    $matches = [];
    // If the filename matches the pattern, return the slug.
    if (preg_match($blade_pattern, $filename, $matches)) {
        return $matches[1];
    }
    // Return FALSE if the filename doesn't match the pattern.
    return false;
}
/**
 * Create blocks based on templates found in Sage's "views/blocks-patterns" directory
 */
add_action('init', function () {

    // Global $sage_error so we can throw errors in the typical sage manner
    global $sage_error;

    // Get an array of directories containing blocks
    $directories = apply_filters('sage-blocks-patterns-folder', []);

    // Check whether ACF exists before continuing
    foreach ($directories as $directory) {
        $dir = \Roots\resource_path($directory);
        

        // Sanity check whether the directory we're iterating over exists first
        if (!file_exists($dir)) {
            return;
        }

        // Iterate over the directories provided and look for templates
        $template_directory = new \DirectoryIterator($dir);

        foreach ($template_directory as $template) {
            if (!$template->isDot() && !$template->isDir()) {
                // Strip the file extension to get the slug
                $slug = removeBladeExtension($template->getFilename());
                // If there is no slug (most likely because the filename does
                // not end with ".blade.php", move on to the next file.
                if (!$slug) {
                    continue;
                }

                // Get header info from the found template file(s)
                $file = "${dir}/${slug}.blade.php";
                $file_path = file_exists($file) ? $file : '';
                $file_headers = get_file_data($file_path, [
                    'title'       => 'Title',
                    'description' => 'Description',
                    'categories'  => 'Categories',
                ]);

                ob_start();
                    echo view('blocks-patterns.' . $slug)->render();
                    $content = ob_get_contents();
                ob_end_clean();

                // Set up block data for registration
                $data = [
                    'title' => $file_headers['title'],
                    'categories' => $file_headers['categories'],
                    'description' => $file_headers['categories'],
                    'content' => $content,
                ];

                //register
                register_block_pattern(
                    'yodio/' . $slug,
                    array(
                        'title'       => $data['title'],
                        'description' => $data['description'],
                        'categories'  => array($data['categories']),
                        'content'     => $data['content'],
                    )
                ); 
            }
        }
    }
});

