<?php

namespace App\Fields\Partials;

use AcfGutenberg\Partial;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Yod\Globals\Acf;

class FontSize extends Partial
{
    /**
     * The partial field group.
     *
     * @return array
     */
    public function fields()
    {
        $fontsize = new FieldsBuilder('font_size');

        $fontsize
            ->addSelect('size',[
                'allow_null' => false,
                'choices' => Acf::$rfs,
                'wrapper' => [ 'width' => '50%' ],
                'default_value' => ['none'],
            ]);

        return $fontsize;
    }
}
