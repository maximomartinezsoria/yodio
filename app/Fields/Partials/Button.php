<?php

namespace App\Fields\Partials;

use AcfGutenberg\Partial;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Yod\Globals\Acf;

class Button extends Partial
{
    /**
     * The partial field group.
     *
     * @return array
     */
    public function fields()
    {
        $button = new FieldsBuilder('button');

        $button
            ->addGroup('button')
                ->addLink('link', [
                    'return_format' => 'array', /* 'array' || 'id' */
                ])
                ->addSelect('style',[
                    'allow_null' => 0,
                    'choices' => Acf::createColorSet('bg'),
                    'default_value' => ['bg-primary'],
                ])
                ->addSelect('color',[
                    'allow_null' => 0,
                    'choices' => Acf::createColorSet('text'),
                    'default_value' => 'text-white',
                ])
                ->addSelect('size',[
                    'allow_null' => 1,
                    'choices' => [
                        'btn-md' => 'Normal',
                        'btn-sm' => 'Small',
                        'btn-lg' => 'Large',
                    ],
                    'default_value' => ['none'],
                ])
            ->endGroup();

        return $button;
    }
}
