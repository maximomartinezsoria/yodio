<?php

namespace App\Fields\Partials;

use AcfGutenberg\Partial;
use StoutLogic\AcfBuilder\FieldsBuilder;

class FontStyle extends Partial
{
    /**
     * The partial field group.
     *
     * @return array
     */
    public function fields()
    {
        $fontstyle = new FieldsBuilder('fontstyle');

        $fontstyle
            ->addSelect('size',[
                'allow_null' => false,
                'choices' => [
                    'h1-set' => 'h1',
                    'h2-set' => 'h2',
                    'h3-set' => 'h3',
                    'h4-set' => 'h4',
                    'h5-set' => 'h5',
                ],
                'wrapper' => [ 'width' => '50%' ],
                'default_value' => ['none'],
            ])
            ->addSelect('color',[
                'allow_null' => false,
                'choices' => [
                    'text-primary' => 'Primary color',
                    'text-secondary' => 'Secondary color',
                    'text-terciary' => 'Terciary color',
                    'text-body-font' => 'Body color',
                    'text-white' => 'White',
                    'text-dark' => 'Black',
                ],
                'wrapper' => [ 'width' => '50%' ],
                'default_value' => ['none'],
            ]);

        return $fontstyle;
    }
}
