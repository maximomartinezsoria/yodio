<?php

// Define variables and data dets

namespace Yod\Globals;

class Acf{

    //Define globals colors datasets
    public static $colors = [
        'primary',
        'secondary',
        'tertiary',
        'gray',
        'white',
        'black',
        'light',
        'cyan',
        'yellow',
        'blue',
        'purple',
        'pink',
        'green',
        'orange',
        'teal'
    ];

    public static $rfs = [
        'rfs-1' => 'Extra small',
        'rfs-2' => 'Small',
        'rfs-3' => 'Medium small',
        'rfs-4' => 'Medium',
        'rfs-5' => 'Medium large',
        'rfs-6' => 'Big small',
        'rfs-7' => 'Big',
        'rfs-8' => 'Big large'
    ];

    public static $border_radius = [
        'rounded-0' => 'No border radius',
        'rounded-1' => '2px',
        'rounded-2' => '4px',
        'rounded-3' => '6px',
        'rounded-4' => '8px',
        'rounded-5' => '10px',
        'rounded-6' => '12px',
        'rounded-7' => '14px',
        'rounded-8' => '16px',
        'rounded-9' => '18px',
        'rounded-10' => '20px',

        
    ];

    //This global arrays come from data.php
    public static $bootstrap_icons = BS_ICONS;

    public static $bootstrap_classes = BS_CLASSES;

    //Define globals colors datasets
    public static $socialnetworks = [
        'facebook',
        'instagram',
        'linkedin',
        'twitter',
        'whatsapp'
    ];

    public static function CreateColorsForGutenberg(){
        $colors = Acf::$colors;
        $palette = [];
    
        foreach($colors as $c){
            $palette[] = [
                'name'  => ucwords($c),
                'slug'  => $c,
                'color' => get_field($c . '_color','option'),
            ];
        }
    
        return $palette;
    }

    public static function CreateColorsForAcf(){
        $colors = Acf::$colors;
        $palette = [];
    
        foreach($colors as $c){
            $palette[] = get_field($c . '_color','option');
        }
    
        return json_encode($palette);
    }


    // Return fonts options
    public static function getFonts(){
        $fonts = [
            'Roboto',
            'Open Sans',
            'Montserrat',
            'Poppins',
            'Nunito',
            'Oswald',
            'Barlow',
            'Barlow Condensed',
            'Work Sans',
            'Heebo',
            'Karla',
            'Inter',
            'Dosis',
            'Lexend',
            'Playfair Display',
            'Lora',
            'Kreon',
            'Bitter',
            'Faustina',
            'Spartan',
            'Jost',
            'Rubik',
            'Manrope',
            'Brygada 1918',
            'Baskerville'
        ];

        asort($fonts);

        return $fonts;
    }

        
    /**
     * Create array with color class and label for using in Acf Selects
     *
     * @param  string $prefix A string that will prepend the color name
     * @return array
     */
    public static function createColorSet($prefix){
        $dataset = [];
        $set = self::$colors;
        //Call colors array and creates the main array
        foreach ($set as $color){
            $dataset[$prefix . '-' . $color] = ucwords($color) . ' Color' ;
        }

        if($prefix == 'bg'){
            $dataset['bg-transparent'] = 'Transparent' ;
        }

        if($prefix == 'border'){
            $dataset['border-transparent'] = 'No border' ;
        }

        return $dataset;
    }

    /**
     * Create array with socials networks based in fonts selected
     *
     * @return array
     */
    public static function createSocialIcons(){
        $dataset = [];
        $socialset = self::$socialnetworks;

        foreach ($socialset as $social){
            $dataset[$social] = ucwords($social);
        }

        return $dataset;
    }

    public static function getSelectIcon(){
        $dataset = [];
        $icons = self::$bootstrap_icons;

        foreach ($icons as $icon){
            $name = str_replace('bi',' ',$icon);
            $dataset[$icon] = "<i class='$icon'></i> " . ucwords(str_replace('-',' ',$name));
        }

        return $dataset;
    }
    
    /**
     * Create arrays for ACF Select based on Bootstrap classes
     *
     * @param  array $type all, margins, paddings, display
     * @return array
     */
    public static function createBsClasses($type = []){
        $classes = [];
        $allclasses = self::$bootstrap_classes;

        if(in_array('margins',$type) || in_array('all',$type)){
            $classes = array_merge($allclasses['margins'],$classes);
        }

        if(in_array('paddings',$type) || in_array('all',$type)){
            $classes = array_merge($allclasses['paddings'],$classes);
        }

        if(in_array('display',$type) || in_array('all',$type)){
            $classes = array_merge($allclasses['display'],$classes);
        }

        if(in_array('alignment',$type) || in_array('all',$type)){
            $classes = array_merge($allclasses['alignment'],$classes);
        }

        if(in_array('positions',$type) || in_array('all',$type)){
            $classes = array_merge($allclasses['positions'],$classes);
        }
        return $classes;
    }

    public static function rowCols($prefix = ''){
        return [
            $prefix . '12' => '12 cols',
            $prefix . '11' => '11 cols',
            $prefix . '10' => '10 cols',
            $prefix . '9' => '9 cols',
            $prefix . '8' => '8 cols',
            $prefix . '7' => '7 cols',
            $prefix . '6' => '6 cols',
            $prefix . '5' => '5 cols',
            $prefix . '4' => '4 cols',
            $prefix . '3' => '3 cols',
            $prefix . '2' => '2 cols',
            $prefix . '1' => '1 col',
        ];
    }
}