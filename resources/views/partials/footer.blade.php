<footer class="footer">
    @query([
        'post_type' => 'block-areas',
        'name' => 'footer'
    ])
    @posts
        @content
    @endposts
</footer>
