<header>
    @query([
        'post_type' => 'block-areas',
        'name' => 'header'
    ])
    @posts
        @content
    @endposts
    
</header>
