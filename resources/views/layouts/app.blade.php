<div class="mx-auto">
  @include('partials.header')

    <main id="main" class="prose main">
      @yield('content')
    </main>

    {{-- @hasSection('sidebar')
      <aside class="sidebar">
        @yield('sidebar')
      </aside>
    @endif --}}

  @include('partials.footer')
</div>
