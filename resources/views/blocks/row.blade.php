@php 
  $count_column = [[ 'core/column']];

  if($columns == 1) $count_column = [[ 'core/column']];
  if($columns == 2) $count_column = [[ 'core/column'],[ 'core/column']];
  if($columns == 3) $count_column = [[ 'core/column'],[ 'core/column'],[ 'core/column']];
  if($columns == 4) $count_column = [[ 'core/column'],[ 'core/column'],[ 'core/column'],[ 'core/column']];

  $template = array(
      ['core/columns',[],$count_column],
  );
  $temp = wp_json_encode( $template);

@endphp
<x-section :block="$block" class="position-relative columns-{{ $columns}} {{ $inner_column_padding }} {{ $inner_column_margin }} {{ $vertical_alignment}} {{ $data_columns }}">
  <InnerBlocks templateLock="all" template="{{ $temp }}"/>
</x-section>
