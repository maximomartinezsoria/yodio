<x-section :block="$block">
  <div>
    <InnerBlocks />
  </div>
</x-section>
