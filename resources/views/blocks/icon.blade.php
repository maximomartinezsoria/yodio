<x-section :block="$block" class="d-flex {{ $align }}">
    <div class="b-icon__wrap d-flex justify-content-center align-items-center {{ $bs_classes }} {{ $style }}" style="background-color: {{ $background }};{{ $sizewrapper }}; {{$set_border}}; border-color: {{ $border }}">
        <i class="b-icon__icon {{ $icon }}" style="font-size: {{ $sizeicon }};color: {{ $color }}"></i>
    </div>
</x-section>
