<x-section :block="$block">
  @if($link)
    <a href="{{ $link['url'] }}" target="" class="btn {{ $style }} {{ $size }}">
      <span class="{{ $color }}">{{ $link['title']}}</span>
    </a>
  @endif
</x-section>