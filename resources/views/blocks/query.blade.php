<x-section :block="$block">
  @if ($items)
  @if($column_width)
    <div class="row">
      <div class="{{ $column_width }}">
  @endif
      <div class="row">
        @foreach ($items as $item)
          <div class="col-md-{{ $columns }} mb-4">
            <x-card :postId="$item" :type="$card_type"/>
          </div>
        @endforeach
      </div>
    @endif
    @if($column_width)
      </div>
    </div>
  @endif
</x-section>
