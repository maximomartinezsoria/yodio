<x-section :block="$block">
    @if($selected_nav)
        <ul class="navbar-nav ms-auto my-3 my-md-0 ">
            @foreach ($selected_nav as $item)
            <li class="nav-item {{ $item->classes ?? '' }} {{ $item->active ? 'active' : '' }}">
                <a class="nav-link {{ $fontweight }} {{$color_link}} {{ $color_hover_link}}" href="{{ $item->url }}" style="font-size: {{$fontsize}}">
                {{ $item->label }}
                </a>
        
                @if ($item->children)
                <ul class="dropdown-menu">
                    @foreach ($item->children as $child)
                    <li class="my-child-item {{ $child->classes ?? '' }} {{ $child->active ? 'active' : '' }}">
                        <a href="{{ $child->url }}">
                        {{ $child->label }}
                        </a>
                    </li>
                    @endforeach
                </ul>
                @endif
            </li>
            @endforeach
        </ul>
    @endif
</x-section>
