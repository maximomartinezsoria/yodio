<x-section :block="$block">
    <nav id="mainNav" data-scrolled="{{ $nav_style_scrolled }}" class="navbar navbar-dark navbar-expand-{{ $expand_at}} nav-{{ $position }} {{ $color_background }}" aria-label="Main navigation">
        <div class="{{ $container }}">

            <a class="navbar-brand zi10" href="{{ get_bloginfo('url') }}">
                @if(isset($logo_desktop['url']))
                    <img src="{{ $logo_desktop['url']}}" alt="{{ get_bloginfo('name') }}">
                @endif
            </a>

            <button class="js-mobile-menu d-{{ $expand_at}}-none hamburger hamburger--squeeze zi10" type="button" aria-label="Menu" aria-controls="navigation">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>

            <div class="navbar-collapse pb-4 pb-md-0 {{ $mobile_background_menu }} expand-{{ $expand_at}} {{ $alignment }}" id="navigationHeader">
                @if($mainnav)
                    <ul class="navbar-nav component-mainnav my-3 my-md-0 ps-md-3">
                        @foreach ($mainnav as $item)
                        <li class="nav-item {{ $item->classes ?? '' }} {{ $item->active ? 'active' : '' }}">
                            <a class="nav-link {{ $fontweight }} {{$nav_color_link}} {{ $nav_color_hover_link}}" href="{{ $item->url }}" style="font-size: {{$fontsize}}">
                            {{ $item->label }}
                            </a>
                    
                            @if ($item->children)
                            <ul class="dropdown-menu">
                                @foreach ($item->children as $child)
                                <li class="my-child-item {{ $child->classes ?? '' }} {{ $child->active ? 'active' : '' }}">
                                    <a href="{{ $child->url }}">
                                    {{ $child->label }}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                            @endif
                        </li>
                        @endforeach
                    </ul>
                @endif

                @if($buttons_navigation)
                    @foreach ($buttons_navigation as $btn)
                       <a class="ms-md-3 btn {{ $btn['button']['size'] }} {{ $btn['button']['style'] }} {{ $btn['button']['color'] }}" href="{{ $btn['button']['link']['url'] }}">{{ $btn['button']['link']['title'] }}</a>
                    @endforeach
                @endif
            </div>
        </div>
    </nav>
</x-section>
