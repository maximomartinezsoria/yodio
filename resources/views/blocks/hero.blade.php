{{-- .b-hero --}}
<x-section 
    :block="$block" 
    class="d-flex align-items-center overflow-hidden position-relative {{ $shadow }} {{ $has_bgimg }} {{ $height }} {{ $shadow }} {{ $content_alignment }}">
    
    @if($overlay_layer)
        <div class="b-hero--layer color overlay-layer zi2 {{$layer_blendmode}}" style='background-color: {{ $overlay_layer_color }}; opacity: {{ $overlay_layer_opacity }}'></div>
    @endif

    @if($has_bgimg != '')
        <div class="b-hero__imagelayer zi1 position-absolute">
            <img src="{{ $bgimg['url']}}" alt="" class="b-hero__image w-100">
        </div>
    @endif

    <div class="container">
        <div class="b-hero__content position-relative zi4">
            <div class="row">
                <div class="col-md-12 mx-auto">

                    @if($innerblock)
                        <InnerBlocks/>
                    @else
                        @if($uppertext != '')
                            <div class="b-hero__uppertext heading-font d-inline-block px-3 py-1 {{ $uppertextstyle}}">{{ $uppertext}}</div>
                        @endif
                        <h1 class="text-white my-3">{!! $heading !!}</h1>

                        <h3 class="text-white my-3">{!! $subheading !!}</h3>

                        <div class="text-white">{!! $content !!}</div>

                        @if(isset($button['link']) && is_array($button['link']))
                            <x-button :button="$button" class="mt-2"/>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-section>
