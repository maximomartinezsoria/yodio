{{-- .b-icon-with-text--}}
<x-section :block="$block">
    <div class="container">
        @if($link)
            <a href="{{$link['url']}}" target="{{ $link['target']}}">
        @endif
        <div class="row align-items-center text-white mb-4 mb-md-0 {{ $iconbg }}">
            <div class="{{ $position_icon}} b-icon-with-text__image d-flex justify-content-center">
                <div class="d-flex justify-content-center align-items-center rounded-circle aspect-ratio-1-1 b-icon-with-text__icon" 
                style="background-color: {{$overlay_layer_color}};width: {{$icon_bg_size}}rem;height: {{ $icon_bg_size}}rem">
                    
                    @if(isset($icon) && isset($icon['url']))
                        <img src="{{ $icon['url']}}" alt="">
                    @endif

                </div>
            </div><!-- .b-icon-with-text__image -->

            <div class="{{ $position_text}} b-icon-with-text__text {{ $position_icon == 'col-12' ? 'text-center' : "" }}">
                <span class="fw-normal">{{ $subtitle }}</span>
                <h3>{{ $title}} </h3>
            </div><!-- .b-icon-with-text__text -->
            
        </div>
        @if($link)
            </a>
        @endif
    </div>
</x-section>


