<x-section :block="$block" class="position-relative {{ $height }} {{ $bg_color }}">
	<div class="{{ $container}} position-relative zi2">
        <div class="{{ $horizontal_alignment }}">
		    <InnerBlocks />
        </div>
	</div>

	@if($image_background && $img_bg['url'] != '')
    <div class="block-background position-absolute overflow-hidden w-100 h-100 zi1" style="top: 0; left: 0; right: 0; bottom: 0;opacity: {{ $image_background_opacity}}">
        <div class="image-container">
            <img class='w-100 object-fit-cover {{ $image_grayscale ? 'fx-grayscale' : '' }}' src="{{ $img_bg['url'] }}">
        </div>
    </div>
  @endif
</x-section>
