@php
  $iconfont = get_field('icon_font','options');

  if($iconfont == 'bootstrap-icons') :
      $icon_class = 'bi bi-';
  elseif($iconfont == 'font-awesome') :
      $icon_class = 'fab fa-';
  endif;

@endphp

<x-section :block="$block">
  <ul class="px-0 {{ $vertical_margins }} d-flex">
  @if($social)
    @foreach($social as $s)
      <li class="li-icon">
        <a class="link-icon me-2 d-flex justify-content-center align-items-center 
          {{ $style_wrapper }} 
          {{ $color_wrapper }} 
          {{ $background_wrapper }} 
          {{ $border }}"
          href="{{ $s['link']['url'] }}">

          <i class="{{ $icon_class . $s['network'] }}"></i>
        </a>
      </li>
    @endforeach
  @endif
  </ul>
</x-section>
