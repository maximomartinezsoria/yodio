{{-- .b-image-with-text --}}
<x-section :block="$block" class="position-relative">
    <div class="container">
        <div class="row {{$align_content}}">
            <div class="col-md-6 {{ $order == "image-text" ? "order-1" : "order-2"}}">
               <x-image :image="$image" size="full" class="component-image-with-text__image w-100 mb-4 mb-md-0 {{ $order == 'image-text' ? 'pe-md-3' : 'ps-md-3'}}" />
            </div>
            <div class="col-md-6 {{ $order == "image-text" ? "order-2" : "order-1"}}">
                <InnerBlocks />
            </div>
        </div>
    </div>
</x-section>
