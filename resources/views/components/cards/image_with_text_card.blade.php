<div class="card mb-3 h-100 image_with_text_card text-white animation-hover my-5 {{ $rounded }}">
    @if ($thumbnail)
        <div class="img-cover">
            <a href="{{ $link }}"><img src="{{$thumbnail}}" alt="{!! $title !!}" class="card-img"></a>
        </div>
        
    @endif

    @if ($taxonomy)
        <div class="card-taxonomy badge bg-primary position-absolute fw-normal">{{ $taxonomy }}</div>
    @endif
    <div class="card-body">
        @if ($date)
            <div class="card-date text-gray">{{ $date }}</div>
        @endif
        <h2 class="{{ $title_size}} mt-2 mb-4 card-title text-dark">{!! $title !!}</h2>
        @if ($description)
            <p class="text-dark">
                {{ $description }}
            </p>
        @endif
        <a href="{{ $link }}" class="btn btn-primary">
            View more
        </a>
    </div>
</div>