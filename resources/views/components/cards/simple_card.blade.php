<div class="card simple-card text-white animation-hover my-5 {{ $rounded }}">
	<a href="{{ $link }}" class="d-block position-relative">
		@if ($thumbnail)
			<img src="{{$thumbnail}}" alt="{!! $title !!}" class="w-100 position-absolute zi1">
		@endif
		<div class="overlay-layer zi2"></div>
		<div class="simple-card__content position-relative align-items-center zi3 d-flex justify-content-center">
			<div class="d-flex align-items-center flex-column">
				@if ($taxonomy)
					<div class="badge bg-primary">{{ $taxonomy }}</div>
				@endif
				@if ($date)
					<div class="card-date">{{ $date }}</div>
				@endif
				<h2 class="{{ $title_size}} card-title text-center">{!! $title !!}</h2>
				@if ($description)
					<p class="text-white fw-normal">
						{{ $description }}
					</p>
				@endif
			</div>
		</div>
	</a>
</div>