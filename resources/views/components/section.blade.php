<section {{ $attributes->merge([
      'class' => $classNames,
      'id' => get_field( 'custom_id' ) ?: $block->id,
    ]) }}>
  {!! $slot !!}
</section>