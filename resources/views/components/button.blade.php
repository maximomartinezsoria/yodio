<a href="{{ $button['link']['url'] }}" target="{{ $button['link']['target'] ?: "" }}" class="btn {{ $buttonClass }} {{ $button['style'] }} {{ $button['color'] }}  {{ $button['size'] }} icon-left">
  <span>{{ $button['link']['title'] ?: $slot }}</span>
</a>
