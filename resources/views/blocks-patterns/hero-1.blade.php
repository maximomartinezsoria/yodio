@php
/*
Title: Hero 1
Categories: hero
Description: This is my description
*/
@endphp
<!-- wp:cover {"url":"https://i.picsum.photos/id/949/1900/1080.jpg?hmac=h9R8p_inOUOI_pMBcYM_eaQ18CZPyXel-5nsvPPVei0","id":310,"dimRatio":70,"customOverlayColor":"#171717","minHeight":750,"contentPosition":"center center","align":"full","className":"is-position-center-center"} -->
<div class="wp-block-cover alignfull has-background-dim-70 has-background-dim is-position-center-center" style="background-color:#171717;min-height:750px"><img class="wp-block-cover__image-background wp-image-310" alt="" src="https://i.picsum.photos/id/949/1900/1080.jpg?hmac=h9R8p_inOUOI_pMBcYM_eaQ18CZPyXel-5nsvPPVei0" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:heading {"textAlign":"center","level":4,"style":{"color":{"text":"#d7d7d7"}}} -->
<h4 class="has-text-align-center has-text-color" style="color:#d7d7d7">Optional Subheading</h4>
<!-- /wp:heading -->

<!-- wp:heading {"textAlign":"center","level":1,"style":{"color":{"text":"#ffffff"}}} -->
<h1 class="has-text-align-center has-text-color" style="color:#ffffff">Main Heading You Can Edit</h1>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center","style":{"color":{"text":"#d7d7d7"}}} -->
<p class="has-text-align-center has-text-color" style="color:#d7d7d7">I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis.</p>
<!-- /wp:paragraph -->

<!-- wp:buttons {"contentJustification":"center"} -->
<div class="wp-block-buttons is-content-justification-center"><!-- wp:button -->
<div class="wp-block-button"><a class="wp-block-button__link">Call to action</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div></div>
<!-- /wp:cover -->