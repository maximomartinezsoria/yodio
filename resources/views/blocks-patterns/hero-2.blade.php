@php
/*
Title: Hero 2
Categories: hero
Description: This is my description
*/
@endphp
<!-- wp:group {"align":"full","style":{"color":{"background":"#171717"}}} -->
<div class="wp-block-group alignfull has-background" style="background-color:#171717"><!-- wp:media-text {"mediaPosition":"right","mediaId":54528,"mediaLink":"https://google.com","mediaType":"image"} -->
<div class="wp-block-media-text alignwide has-media-on-the-right is-stacked-on-mobile"><figure class="wp-block-media-text__media"><img src="https://i.picsum.photos/id/206/900/900.jpg?hmac=DZ78dRFmOJ-4rTLL1rHTvbBiw6-0IGK3PR_4OsxX28k" alt="" class="wp-image-54528 size-full"/></figure><div class="wp-block-media-text__content"><!-- wp:heading {"textAlign":"center","level":1,"align":"full","style":{"color":{"text":"#ffffff"}}} -->
<h1 class="alignfull has-text-align-center has-text-color" style="color:#ffffff">This is the Main Heading You Can Edit</h1>
<!-- /wp:heading -->
<!-- wp:paragraph {"align":"center","style":{"color":{"text":"#d7d7d7"}}} -->
<p class="has-text-align-center has-text-color" style="color:#d7d7d7">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
<!-- /wp:paragraph -->
<!-- wp:buttons {"contentJustification":"center"} -->
<div class="wp-block-buttons is-content-justification-center"><!-- wp:button -->
<div class="wp-block-button"><a class="wp-block-button__link">Call to Action</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div></div>
<!-- /wp:media-text --></div>
<!-- /wp:group -->