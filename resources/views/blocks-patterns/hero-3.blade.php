@php
/*
Title: Hero 3
Categories: hero
Description: This is my description
*/
@endphp
<!-- wp:group {"align":"full","style":{"color":{"background":"#171717"}}} -->
<div class="wp-block-group alignfull has-background" style="background-color:#171717"><!-- wp:heading {"textAlign":"center","level":1,"style":{"color":{"text":"#ffffff"}}} -->
<h1 class="has-text-align-center has-text-color" style="color:#ffffff">This Is the Main Heading<br> Lorem ipsum ddolor sit</h1>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center","style":{"color":{"text":"#d7d7d7"}}} -->
<p class="has-text-align-center has-text-color" style="color:#d7d7d7">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper <br>mattis, pulvinar dapibus leo.</p>
<!-- /wp:paragraph -->

<!-- wp:buttons {"contentJustification":"center"} -->
<div class="wp-block-buttons is-content-justification-center"><!-- wp:button -->
<div class="wp-block-button"><a class="wp-block-button__link" href="#">Call to Action</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons -->

<!-- wp:spacer {"height":30} -->
<div style="height:30px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:image {"align":"center","id":54618,"sizeSlug":"large","linkDestination":"none"} -->
<div class="wp-block-image"><figure class="aligncenter size-large"><img src="https://i.picsum.photos/id/446/1100/420.jpg?hmac=myQhWgeluilyaZj9CzT3hF8N9LTd-V8g_ktWq1w4ZEU" alt="" class="wp-image-54618"/></figure></div>
<!-- /wp:image --></div>
<!-- /wp:group -->