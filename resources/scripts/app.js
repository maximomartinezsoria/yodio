/**
 * External Dependencies
 */
import 'jquery';
import 'bootstrap';

import onScroll from './components/onScroll';
import mobileMenu from './components/mobileMenu';

$(function () {
  onScroll();
  mobileMenu();
});
