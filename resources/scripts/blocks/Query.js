export default function Query()
{
    if (typeof acf !== 'undefined') {
        // let fn_originalAjaxData = false;

        acf.add_filter('select2_ajax_data', (data, args, $input, field) => {
            

            // Only execute if we are choosing the "Taxonomy Name" select field
            if (data.field_key === 'field_query_tax_query_tax_queries_taxonomy_name' || data.field_key === 'field_query_taxonomy_field') {
                
                //Here check 
                data.post_type = $('.acf-field-query-post-type').find('select').select2('val');
                
            }

            // Only execute if we are choosing the "Selected Terms" select field
            if (data.field_key === 'field_query_tax_query_tax_queries_selected_terms') {

                // Look for input ID
                let termSelectId = $input.attr('id');
                // console.log( termSelectId );
                let idInCommon = termSelectId.replace('-field_query_tax_query_tax_queries_selected_terms', '');
                let newSelect = '#' + idInCommon + '-field_query_tax_query_tax_queries_taxonomy_name';

                data.taxonomy_name = jQuery(newSelect).select2('val');
            }

            console.log(data.post_type);

            return data;

            
        });
    }
}

window.addEventListener('DOMContentLoaded', (event) => {
    Query(); 
});
