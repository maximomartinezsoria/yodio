const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {Fragment } = wp.element;
const { InnerBlocks, InspectorControls, MediaUpload, MediaUploadCheck, useSetting } = wp.blockEditor;
const { PanelBody, SelectControl, Button, ResponsiveWrapper } = wp.components;

registerBlockType( 'yodio/section-block', {

    title: 'Section Block',
    icon: 'editor-table',
    category: 'layout',
    keywords: ['section'],  

    attributes: {
		mediaID: {
            type: 'number',
            default: 0
        },
        mediaURL: {
			type: 'string'
		},
	},

    // Will be implemented afterwards
    edit: ( { attributes, setAttributes} ) => {

        const { mediaID, mediaURL, className } = attributes;
    
        //extract colors from theme.json
        const customColors = useSetting( 'color.palette' );

        const colorSet = customColors.map(color => ({
            value: `bg-${color.slug}`,
            label: `${color.name} Color`
        }))

        const removeMedia = () => {
            setAttributes({
                mediaID: 0,
                mediaURL: ''
            });
        }
    
        const onSelectImage = (media) => {
            setAttributes({
                mediaID: media.id,
                mediaURL: media.url
            });
        }
    
        const blockStyle = {
            backgroundImage: !! mediaURL ? 'url(' + mediaURL + ')' : 'none'
        };
    
        return (
            <Fragment>
                <InspectorControls>
                    <PanelBody
                        title="Background styles"
                        initialOpen={ true }
                    >
                        <SelectControl
                            label= 'Background Color'
                            value={ className }
                            options={ colorSet }
                            onChange={ ( selectedOption ) => setAttributes( { className: selectedOption } ) }
                        />

                        <div className="wp-block-image-selector-example-image">
                            { ! mediaID  &&
                            <MediaUploadCheck>
                                <MediaUpload
                                    title='Background image'
                                    onSelect={ onSelectImage }
                                    value={ mediaID }
                                    render={ ( { open } ) => (
                                        <Button
                                            className={ 'editor-post-featured-image__toggle' }
                                            onClick={ open }>
                                            Set background image
                                        </Button>
                                    ) }
                                />
                            </MediaUploadCheck>
                            }

                            { !! mediaID &&
                                <ResponsiveWrapper naturalWidth={ 400 } naturalHeight={ 200 }>
                                    <img src={ mediaURL } alt='Background image'/>
                                </ResponsiveWrapper>
                            }
                            { !! mediaID &&
                                <MediaUploadCheck>
                                    <Button onClick={ removeMedia } isLink isDestructive>
                                        Remove background image
                                    </Button>
                                </MediaUploadCheck>
                            }
                        </div>
                    </PanelBody>
                </InspectorControls>
                <div
                    className={`block-section ${className}`}
                >
                    <div className="block-section__overlayer" style={blockStyle}>
                            
                    </div>
                    
                    <div className="block-section__content">
                        <InnerBlocks />
                    </div>
                </div>
            </Fragment>
        );
    },

    // Will be implemented afterwards
    save: ( { attributes } ) => {

        const { mediaID, mediaURL, className } = attributes;

        const blockStyle = {
            backgroundImage: !!mediaURL ? 'url(' + mediaURL + ')' : 'none'
        };

        return (
            <div
                className={`block-section ${className || ''}`}
            >
                <div className="block-section__overlayer" style={blockStyle}>
                        
                </div>
                
                <div className="block-section__content">
                    <InnerBlocks.Content />
                </div>
            </div>
        );
	}
} );