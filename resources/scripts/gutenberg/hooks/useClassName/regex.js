export const classNameRegex = {
  text: new RegExp(/text-[\w-]*[ ]?/, 'g'),
  fontWeight: new RegExp(/fw-\w*[ ]?/, 'g'),
  lineHeight: new RegExp(/lh-\w*[ ]?/, 'g'),
  paddings: new RegExp(/py-\w\w-\d[ ]?/, 'g'),
};
