export default function useClassName(
  attributes,
  classNamesData,
  regularExpressions
) {
  const newClassName = classNamesData
    .map(createNewClassName)
    .filter(emptyString)
    .join(' ');

  const userSetClassName = regularExpressions.reduce((className, regex) => {
    return className.replace(regex, '');
  }, attributes.className);

  attributes.className = `${newClassName} ${userSetClassName}`;
}

function emptyString(string) {
  return string.length;
}

function createNewClassName({ prefix, value, breakpoints }) {
  if (breakpoints) {
    return Object.keys(value)
      .reduce((className, breakpoint) => {
        return `${className}${prefix}${breakpoint}-${value[breakpoint]} `;
      }, '')
      .trim();
  }

  return value ? `${prefix}${value}` : '';
}

export * from './regex';
