const { addFilter } = wp.hooks;
const { createHigherOrderComponent } = wp.compose;
const { Fragment } = wp.element;
const { InspectorControls } = wp.blockEditor;
const { PanelBody, SelectControl } = wp.components;
import useClassName, { classNameRegex } from './hooks/useClassName';

const enableTypographyControlOnBlocks = ['core/paragraph'];

const addTypographyControlAttribute = (settings, name) => {
  if (!enableTypographyControlOnBlocks.includes(name)) {
    return settings;
  }

  const settingsAtributes = settings?.attributes || {};

  settings.attributes = {
    ...settingsAtributes,
    ...{
      transform: {
        type: 'string',
        default: '',
      },
      weight: {
        type: 'string',
        default: '',
      },
      lineHeight: {
        type: 'string',
        default: '',
      },
      decoration: {
        type: 'string',
        default: '',
      },
    },
  };

  return settings;
};

addFilter(
  'blocks.registerBlockType',
  'extend-block/attribute/typography',
  addTypographyControlAttribute
);

const withTypographyControl = createHigherOrderComponent((BlockEdit) => {
  return (props) => {
    if (!enableTypographyControlOnBlocks.includes(props.name)) {
      return <BlockEdit {...props} />;
    }
    const { transform, weight, lineHeight, decoration } = props.attributes;
    useClassName(
      props.attributes,
      [
        {
          value: transform,
          prefix: 'text-',
        },
        {
          value: weight,
          prefix: 'fw-',
        },
        {
          value: lineHeight,
          prefix: 'lh-',
        },
        {
          value: decoration,
          prefix: 'text-decoration-',
        },
      ],
      [
        classNameRegex.text,
        classNameRegex.lineHeight,
        classNameRegex.fontWeight,
      ]
    );

    return (
      <Fragment>
        <BlockEdit {...props} />
        <InspectorControls>
          <PanelBody title={'Typography'} initialOpen={true}>
            <SelectControl
              label="Transform"
              value={transform}
              onChange={(transform) => {
                props.setAttributes({ transform });
              }}
              options={[
                { value: '', label: 'Select transform' },
                { value: 'lowercase', label: 'Lowercase' },
                { value: 'uppercase', label: 'Uppercase' },
                { value: 'capitalize', label: 'Capitalize' },
              ]}
            />
            <SelectControl
              label="Weight"
              value={weight}
              onChange={(weight) => {
                props.setAttributes({ weight });
              }}
              options={[
                { value: '', label: 'Select weight' },
                { value: 'lighter', label: 'Lighter' },
                { value: 'light', label: 'Light' },
                { value: 'normal', label: 'Normal' },
                { value: 'bold', label: 'Bold' },
                { value: 'bolder', label: 'Bolder' },
              ]}
            />
            <SelectControl
              label="Line Height"
              value={lineHeight}
              onChange={(lineHeight) => {
                props.setAttributes({ lineHeight });
              }}
              options={[
                { value: '', label: 'Select line height' },
                { value: '1', label: '1' },
                { value: 'sm', label: 'SM' },
                { value: 'base', label: 'Base' },
                { value: 'lg', label: 'LG' },
              ]}
            />
            <SelectControl
              label="decoration"
              value={decoration}
              onChange={(decoration) => {
                props.setAttributes({ decoration });
              }}
              options={[
                { value: '', label: 'Select decoration' },
                { value: 'underline', label: 'Underline' },
                { value: 'line-through', label: 'Line through' },
              ]}
            />
          </PanelBody>
        </InspectorControls>
      </Fragment>
    );
  };
}, 'withTypographyControl');

addFilter(
  'editor.BlockEdit',
  'extend-block/attribute/typography',
  withTypographyControl
);
