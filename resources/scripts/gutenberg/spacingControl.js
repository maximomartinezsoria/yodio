import { useState } from 'react';
<<<<<<< HEAD
=======
import useClassName, { classNameRegex } from './hooks/useClassName';
>>>>>>> 5e8773b3e2ddc5215d9fb28359e3f5b8e64574b1
const { addFilter } = wp.hooks;
const { createHigherOrderComponent } = wp.compose;
const { Fragment } = wp.element;
const { InspectorControls } = wp.blockEditor;
const { PanelBody, RangeControl, ButtonGroup, Button } = wp.components;

const enableSpacingControlOnBlocks = ['core/paragraph'];

const addSpacingControlAttribute = (settings, name) => {
  if (!enableSpacingControlOnBlocks.includes(name)) {
    return settings;
  }

  const settingsAtributes = settings?.attributes || {};

  settings.attributes = {
    ...settingsAtributes,
    ...{
<<<<<<< HEAD
      spacing: {
        type: 'number',
        default: 0,
=======
      spacings: {
        type: 'object',
        default: { sm: 0, md: 0, lg: 0 },
>>>>>>> 5e8773b3e2ddc5215d9fb28359e3f5b8e64574b1
      },
    },
  };

  return settings;
};

addFilter(
  'blocks.registerBlockType',
  'extend-block/attribute/spacing',
  addSpacingControlAttribute
);

const withSpacingControl = createHigherOrderComponent((BlockEdit) => {
  return (props) => {
    const [selectedBreakpoint, setSelectedBreakpoint] = useState('sm');
    if (!enableSpacingControlOnBlocks.includes(props.name)) {
      return <BlockEdit {...props} />;
    }
<<<<<<< HEAD

    const spacings = props.attributes.spacings || { sm: 0, md: 0, lg: 0 };
    let paddingClassName = '';
    Object.keys(spacings).forEach((key) => {
      paddingClassName += `py-${key}-${spacings[key]} `;
    });

    const currentClassName = props.attributes.className;
    const className = currentClassName?.replace(/py-\w\w-\d[ ]?/g, '') || '';
    props.attributes.className = `${paddingClassName}${className}`;
=======
    const { spacings } = props.attributes;
    useClassName(
      props.attributes,
      [
        {
          value: spacings,
          prefix: 'py-',
          breakpoints: true,
        },
      ],
      [classNameRegex.paddings]
    );
>>>>>>> 5e8773b3e2ddc5215d9fb28359e3f5b8e64574b1

    return (
      <Fragment>
        <BlockEdit {...props} />
        <InspectorControls>
          <PanelBody title={'Spacing Control'} initialOpen={true}>
            <ButtonGroup>
              <Button
                variant="primary"
                isPressed={selectedBreakpoint === 'sm'}
                onClick={() => setSelectedBreakpoint('sm')}
              >
                SM
              </Button>
              <Button
                variant="primary"
                isPressed={selectedBreakpoint === 'md'}
                onClick={() => setSelectedBreakpoint('md')}
              >
                MD
              </Button>
              <Button
                variant="primary"
                isPressed={selectedBreakpoint === 'lg'}
                onClick={() => setSelectedBreakpoint('lg')}
              >
                LG
              </Button>
            </ButtonGroup>
            <RangeControl
              min={0}
              max={5}
              step={1}
              onChange={(selectedOption) => {
                props.setAttributes({
                  spacings: {
                    ...spacings,
                    [selectedBreakpoint]: selectedOption,
                  },
                });
              }}
              label={'Spacing'}
              value={spacings[selectedBreakpoint]}
            />
          </PanelBody>
        </InspectorControls>
      </Fragment>
    );
  };
}, 'withSpacingControl');

addFilter(
  'editor.BlockEdit',
  'extend-block/attribute/spacing',
  withSpacingControl
);
