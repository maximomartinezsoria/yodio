function mobileMenu(){

    let el = document.getElementsByClassName('js-mobile-menu');
    let menu = document.getElementById('navigationHeader');

    el[0].addEventListener('click',function(){
        el[0].classList.toggle('is-active');

        menu.classList.contains('open') ? menu.classList.remove('open') : menu.classList.add('open');
        
    })

}

export { mobileMenu as default };