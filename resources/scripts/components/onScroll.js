function onScroll() {
  window.onscroll = function () {
    changeNav();
  };

  function changeNav() {
    let nav = document.getElementById('mainNav');
    let navfixed = nav.classList.contains('nav-fixed-top')

    if(navfixed){
        //Dynamic style from ACF Block
        let navClass = nav.dataset.scrolled;
        let scroll = document.documentElement.scrollTop;
        let height = nav.offsetHeight;

        if (scroll > height && scroll < height + 50) {
            nav.classList.add(navClass,"has-fx-scrolled");
        } else if(scroll > height + 50){
            nav.classList.add("show");
        } else if(scroll < 20) {
            nav.classList.remove(navClass,"has-fx-scrolled","show");
        }        
    }

  }
}
export { onScroll as default };
