<?php

$variables = [
    //Colors
    'bs-primary' => get_field('primary_color', 'option') .' !important' ?: "#6610f2",
    'bs-secondary' => get_field('secondary_color', 'option') .' !important' ?: "#0dcaf0",
    'bs-tertiary' => get_field('terciary_color', 'option') .' !important' ?: "#000000",
    'bs-blue' => get_field('blue_color','options') .' !important' ?: "#0d6efd",
    'bs-red' => get_field('red_color','options') .' !important' ?: "#dc3545",
    'bs-orange' => get_field('orange_color','options') .' !important' ?: "#fd7e14",
    'bs-yellow' => get_field('yellow_color','options') .' !important' ?: "#ffc107",
    'bs-green' => get_field('green_color','options') .' !important' ?: "#198754",
    'bs-teal' => get_field('teal_color','options') .' !important' ?: "#20c997",
    'bs-cyan' => get_field('cyan_color','options') .' !important' ?: "#0dcaf0",
    'bs-gray' => get_field('gray_color','options') .' !important' ?: "#adb5bd",
    'bs-white' => get_field('white_color','options') .' !important' ?: "#ffffff",
    'bs-purple' => get_field('purple_color','options') .' !important' ?: "#6f42c1",
    'bs-pink' => get_field('pink_color','options') .' !important' ?: "#d63384",
    'bs-black' => get_field('black_color','options') .' !important' ?: "#333333",
    'bs-light' => get_field('light_color', 'option') .' !important' ?: "#fff",
    'text_color' => get_field('text_color', 'option') .' !important' ?: "#333333",
    'link_color' => get_field('link_color', 'option') .' !important' ?: "#6610f2",
    'body_fontweight' => get_field('body_fontweight', 'option') ?: "400",

    //Aditional
    'body_fontsize' => (get_field('body_fontsize', 'option') ?: "17" ) . 'px',
    'btn_rounded' => (get_field('btn_rounded', 'option') ?: "0") . 'px'
];

//Type settings

/**
 * Add px if a element of the setting have size in their index
 *
 * @param  array $h The group of settings for the type
 * @return void
 */
function renderSizeSettings($h){
    $vars = [];
    if(is_array($h)) :
        foreach($h as $i => $hrows) : 
            if(strpos($i,'_size') !== false)
            {
                $vars[$i] = $hrows . 'px';
            } elseif(strpos($i,'_letterspacing') !== false)
            {
                if($hrows == 0) :
                    $vars[$i] = 'normal';
                else :
                    $vars[$i] = $hrows . 'px';
                endif;
            } elseif(strpos($i,'_rem') !== false)
            {
                $vars[$i] = $hrows . 'rem';
            } elseif(strpos($i,'_vw') !== false){
                    $vars[$i] = $hrows . 'vw';
            } else {
                $vars[$i] = $hrows;
            }
        endforeach;
    endif;

    return $vars;
}

$variables = array_merge(
    $variables,
    renderSizeSettings(get_field('h1','option')),
    renderSizeSettings(get_field('h2','option')),
    renderSizeSettings(get_field('h3','option')),
    renderSizeSettings(get_field('h4','option')),
    renderSizeSettings(get_field('h5','option')),
    renderSizeSettings(get_field('h6','option')),
    renderSizeSettings(get_field('btn_sm', 'option')),
    renderSizeSettings(get_field('btn_md', 'option')),
    renderSizeSettings(get_field('btn_lg', 'option')),
    renderSizeSettings(get_field('gutenberg_fontsizes','option')),
);

//Fonts
if(get_field('heading_font', 'option')) :
    $font = get_field('heading_font', 'option') ?: 'Helvetica,Arial';

    $variables['heading_font'] = $font;
    echo '<link href="https://fonts.googleapis.com/css2?family='.urlencode($font).':wght@300;400;500;600;700&display=swap" rel="stylesheet">';
endif;

if(get_field('text_font', 'option')) :
    $font = get_field('text_font', 'option');

    $variables['text_font'] = $font;
    echo '<link href="https://fonts.googleapis.com/css2?family='.urlencode($font).':wght@300;400;500;600;700&display=swap" rel="stylesheet">';
else :
    $variables['text_font'] = 'Helvetica,Arial';
endif;

if(get_field('icon_font', 'option')) :
    $iconfont = get_field('icon_font', 'option');
    if($iconfont == 'font-awesome') :
        echo '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />';
    endif;
endif;

if(get_field('btn_font', 'option')) :
    $font = get_field('btn_font', 'option');

    $variables['btn_font'] = $font;
    echo '<link href="https://fonts.googleapis.com/css2?family='.urlencode($font).':wght@300;400;700&display=swap" rel="stylesheet">';
else :
    $variables['btn_font'] = 'Helvetica,Arial';
endif;

//Icons fonts
if(get_field('icon_font', 'option')) :
    $icons = get_field('icon_font', 'option');

    switch ($icons) :
        case 'bootstrap-icons';
            echo '<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">';
            break;
        
    endswitch;
endif;

//Button hover
$btn = get_field('btn_hover','option');

$variables['btn_hovershadow'] = (isset($btn['shadow']) && $btn['shadow']) ? '0.3' : '0';
$variables['btn_brightness'] = (isset($btn['brightness']) && $btn['brightness']) ? '1.2' : '1';
$variables['btn_hovergrow'] = (isset($btn['grow']) && $btn['grow']) ? '1.02' : '1';

?>

<style>
  :root {
    <?php foreach ($variables as $key => $value) : ?>--<?= $key ?>: <?= $value ?>;
    <?php endforeach ?>
    --clamp_pref_width: 8vw;
  }
</style>